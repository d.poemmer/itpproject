# Projekttagebuch

## W01
- Repository eingerichtet
- User Stories erstellt
- Zeitaufwand: 2h

## W02
- Userstories erweitert
- Planung des Projektdetails

## W03
- Scrum Planung fertigstellen + Review der User-Stories
- Mockups um das Design der Funktionen darzustellen
    -Auf Zettel vorzeichen
    -In digitales Medium übertragen

## W04
- Mockups der Mobile-App in Adobe XD fertiggestellt

## W05
- Zeiteinplanung in GitLab
- Einführung in ExpressJS und MongoDB

## W06
Vorbereitung Sprint Abnahme
- Protokoll erstellen
- Erste Schritte in MongoDB und Node JS

## W07
- Sprintabnahme

## W08
- Sprintplanung
- Datenbank testen

## W09
- Login-System implementieren

# W10
- Frontent verbessert
- Login-System erweitert
- Protokoll für Sprintabnahme erstellt

## W11
- Sprint 2 Abnahme
- Planung des Sprints 3

## W12
- Frontent für das Hinzufügen
    - Notifications
    - Transaktionen 

## W13
- Frontent für das Hinzufügen/Löschen der
    - Notifications
    - Transaktionen fertigstellen
## W14
- Backend für 
    - Notifications
    - Transaktionenen implementiert

## W15
- Tabelle für Notifikationen und Transaktionen dynamisch laden

## W16
- Planung des Sprints
- Einteilung der Tasks
- Besprechen der Ziele

## W17
- Sprintabnahme
- Erstellung des Portfolio-Pie-Chart

## W18
- Behebung der Fehler des Pie-Charts

## W19
- Darstellung des Charts verfeinert und kleine Verbesserungen

## W20
- Bugs beheben

## W21
- Bugs beheben fertigstellen

## W22
- Timeframe Buttons (aktiven Button highlighten)
- Automatische Preis-Updates

## W23
- Aktualisierung der Preise und anderer Infos im Hintergrund

## W24
- Aktualisierung fertigstellen

## W25
- Farbpalette erstellen

## W26
- Abnahme Sprint 5
- Planung des letzten Sprints

## W27
- Ausblendung der Tabelle bei nicht vorhandenen Transaktinen und Notificaitons
- Versuch des Bugfixings beim Löschen der Transaktionen

## W28
- Design-Verbesserungen im Frontend der Asset-Page
- Bug fixing bei Löschen der Transaktionen

## W29
- Ausblendung der Transaktionen und Notifikationen bei leerer Tabelle
- Styling der Single-Asset Page
