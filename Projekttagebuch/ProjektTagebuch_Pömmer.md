# Projekttagebuch

# Sprint 1

## W01
- GitLab/Repository eingerichtet
- Templates erstellt
- Zeitaufwand: 2h

## W02
- Backlog erstellt
- User Stories erweitert

## W03
- Scum Planung fertigstellen + Review
- API calls einrichten (nicht fertig)

## W04
- API Test Page verbessert (Code Cleanup)
- Methode gesucht, um Methoden nacheinander ausführen zu können (nicht fertig)

## W05
- Zeiteinplanung der Arbeitspakete
- API Test Page: 
  - Portfoliopreise und Marketpreise fertiggestellt
  - Preise mit Methode in einem Zeitintervall bekommen (nicht fertig)

## W06
- API Test Page fertig gestellt
- Sprint Abnahme vorbereitet
- informiert über ExpressJS/MongoDB

## W07
- krank

# Sprint 2

## W08
- Sprint 2 Planung
- MongoDB Test

## W09
- Backend 
  - User Registrierung/Login Methoden programmiert
  - Datenbank zugriff möglich

## W10
- Backend fertig gestellt
- Funktionen der Test Page eingebunden
- Sprint 2 Abnahme vorbereitet

## W11
- Sprint 2 Abnahme
- Sprint 3 geplant
- Portfolio:
  - Anzeige der Holdings in einer Tabelle
  - Aktueller Wert angezeigt
  - Chart wird angezeigt (Ohne Berücksichtigung der Transaktionen)

# Sprint 3

## W12
- Portfolio Preis fertiggestellt
- Favoriten hinzufügen Frontend Logik (confirm dialog fehlt noch)
- Chart Timeframe Änderungsbuttons Frontend

## W13
- Confirm Dialog für Entfernen von Favoriten erstellt
- Notifications/Transactions Frontend gefixt

## W14
- Tabelle für Transaktionen und Notifications pro Kryptowährung: Dynamisches einfügen und Event zum löschen der Einträge

## W15
- Sprint 3 Abnahme Vorbereitung
- Sprint 4 Planung
- Angefangen mit Account Page

# Sprint 4

## W16
- Sprintabnahme
- Account Page Layout fast fertig gestellt

## W17
- Probleme mit Repository gelöst
- Account Page: Username und Email dynamisch geladen

## W18
- Delete Account Button funktioniert
- Change Password Button funktioniert

## W19
- Account Page Funktionen fertig gemacht
  - Changing Email
  - Changing Username
  - Changing Default Currency

## W20
- Sprint 4 Abnahme
- Sprint 5 geplant
- %-Veränderung nach gewähltem Timeframe

# Sprint 5

## W21
- Probleme mit prozentualer Veränderung gelöst (wurde falsch berechnet und falsch formatiert)

## W22
- Anfangen mit der Funktion zur Berechnung des Portfoliocharts mit Berücksichtigung der Transaktionen

## W23
- Funktion zur Berechnung des Portfoliocharts mit Berücksichtigung der Transaktionen fertiggestellt (+Togglebutton implementiert)

## W24
- Bug fixing der Aktualisierung der Chart und Coin Daten
- Bug fixing, um den aktiven Button der Timeframes zu bestimmen
- Portfolio Chart Refreshing (every 1 minute)

## W25
- Sprint 5 Abnahme Vorbereitung
- Pie Chart Background Color fixing
- CSS styling (chart Größe, consider-tx-button position)

# Sprint 6

## W26
- Sprint 5 Abnahme
- Planung für Sprint 6 (letzter Sprint)

## W27
- Daten Aktualisierungen bei AssetPage+Portfolio page behoben (+button)
- Bugfixing
- diverse Design Überarbeitungen

## W28
- Register Button gefixt (Modal hat sich vorher geschlosseb, auch bei falscher Eingabe)
- Asset Page Aktualisierungsinvervall gefixt
- Add Transaction/Add Notification Modal verschönert
- Notification/Transaction button init: Fehler (nicht fertig)

## W29
- Mit Präsentation begonnen
- Bugfixing (Notification und TX Liste)

## W30
- Präsentation weitergemacht (noch nicht fertig)