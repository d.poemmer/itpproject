# Scrum Meeting 12
- Datum: 19.01.2021

## Was wurde gemacht?
### Pömmer:
- Portfolio Preis fertiggestellt
- Favoriten hinzufügen Frontend Logik (confirm dialog fehlt noch)
- Chart Timeframe Änderungsbuttons Frontend
### Faltin:
- Frontent für das Hinzufügen/Löschen der
    - Notifications
    - Transaktionen fertigstellen

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Confirm Dialog für Favoriten entfernung
- Automatische Aktualisierung der Chartdaten
### Faltin:
- Backend für 
    - Notifications
    - Transaktionenen implementieren