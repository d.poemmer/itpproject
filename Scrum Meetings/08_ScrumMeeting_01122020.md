# Scrum Meeting 08
- Datum: 01.12.2020

## Was wurde gemacht?
### Pömmer:
- Sprint 2 Planung
- MongoDB Test, Informiert über Express
### Faltin:
- Sprintplanung
- Datenbank testen

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Backend (User Registrierung/Login Methoden Programmieren, Datenbank zugriff regeln)
### Faltin:
- User-Schema der Datenbank erstellen