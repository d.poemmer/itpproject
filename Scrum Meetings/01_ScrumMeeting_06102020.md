# Scrum Meeting 01
- Datum: 06.10.2020

## Was wurde gemacht?
### Pömmer:
- GitLab/Repository eingerichtet
- Templates erstellt
### Faltin:
- Repository eingerichtet
- User Stories erstellt

## Wo gabs Probleme?
### Pömmer:
- keine Probleme
### Faltin:
- keine Probleme

## Was wird gemacht?
### Pömmer:
- Backlog erstellen
### Faltin:
- Überarbeitung der User Stories