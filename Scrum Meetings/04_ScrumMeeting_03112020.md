# Scrum Meeting 00
- Datum: 03.11.2020

## Was wurde gemacht?
### Pömmer:
- API Test Page verbessert (Code Cleanup)
- Methode gesucht, um Methoden nacheinander ausführen zu können (nicht fertig)
### Faltin:
- Mockups der Mobile-App in Adobe XD fertiggestellt

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- API Test Page: API calls fertigstellen
- Backend Planung
### Faltin:
- Backend Planung
- Einführung in Express-JS & MongoDB