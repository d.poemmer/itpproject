# Scrum Meeting 10
- Datum: 22.12.2020

## Was wurde gemacht?
### Pömmer:
- Backend fertig gestellt
- Funktionen der Test Page eingebunden
- Sprint 2 Abnahme vorbereitet
### Faltin:
- Frontent verbessert
- Login-System erweitert
- Protokoll für Sprintabnahme erstellt

## Wo gabs Probleme?
### Pömmer:
- kein
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Sprintabnahme
- Planung Sprint 2
### Faltin:
- Sprintabnahme
- Planung Sprint 2