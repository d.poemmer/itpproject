# Scrum Meeting 28
- Datum: 25.05.2021

## Was wurde gemacht?
### Pömmer:
- Register Button gefixt (Modal hat sich vorher geschlosseb, auch bei falscher Eingabe)
- Asset Page Aktualisierungsinvervall gefixt
- Add Transaction/Add Notification Modal verschönert
- Notification/Transaction button init: Fehler (nicht fertig)
### Faltin:
- Design-Verbesserungen im Frontend der Asset-Page
- Bug fixing bei Löschen der Transaktionen

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Präsentation beginnen
### Faltin:
- Ausblendung der Transaktionen und Notifikationen bei leerer Tabelle
- Styling der Single-Asset Page