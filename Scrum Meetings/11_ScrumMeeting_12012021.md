# Scrum Meeting 11
- Datum: 12.01.2021

## Was wurde gemacht?
### Pömmer:
- Sprint 2 Abnahme
- Sprint 3 geplant
- Portfolio:
  - Anzeige der Holdings in einer Tabelle
  - Aktueller Wert angezeigt
  - Chart wird angezeigt (Ohne Berücksichtigung der Transaktionen)
### Faltin:
- Frontend für das Hinzufügen/Löschen der
    - Notifications
    - Transaktionen

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Portfolio Preis fertigstellen
- Favoriten hinzufügen Frontend Logik
- Chart Timeframe ändern Buttons
### Faltin:
- Frontent für das Hinzufügen/Löschen der
    - Notifications
    - Transaktionen fertigstellen