# Scrum Meeting 23
- Datum: 20.04.2021

## Was wurde gemacht?
### Pömmer:
- Funktion zur Berechnung des Portfoliocharts mit Berücksichtigung der Transaktionen fertiggestellt (+Togglebutton implementiert)
### Faltin:
- Aktualisierung der Preise und anderer Infos im Hintergrund

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Bug fixing / Cleanup
### Faltin:
- Updates ferigstellen