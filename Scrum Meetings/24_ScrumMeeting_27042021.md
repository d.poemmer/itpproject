# Scrum Meeting 27
- Datum: 27.04.2021

## Was wurde gemacht?
### Pömmer:
- Bug fixing der Aktualisierung der Chart und Coin Daten
- Bug fixing, um den aktiven Button der Timeframes zu bestimmen
- Portfolio Chart Refreshing (every 1 minute)
### Faltin:
- Aktualisierung der Preis-Daten sowie diverse Informationen der Coins
- Bei Portfolio Daten neu laden nach eienm Intervall
- Neuzeichnung der Charts nach gewissen Zeitabständen

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Bug-Fixing / Cleanup
- Sprint Abgabe Vorbereitung
### Faltin:
- Color-Palette erstellen zur schöneren Darstellung des Pie-Charts anstatt zufälliger Werte