# Scrum Meeting 04
- Datum: 10.11.2020

## Was wurde gemacht?
### Pömmer:
- Zeiteinplanung der Arbeitspakete
- API Test Page: 
  - Portfoliopreise und Marketpreise fertiggestellt
  - Preise mit Methode in einem Zeitintervall bekommen (nicht fertig)
### Faltin:
- Zeiteinplanung in GitLab
- Einführung in ExpressJS und MongoDB

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Vorbereitung Sprint Abnahme
- Methode, bei der man Preise in einem Zeitintervall bekommt fertigstellen
### Faltin:
- Vorbereitung Sprint Abnahme
- Protokoll erstellen
- Erste Schritte in MongoDB