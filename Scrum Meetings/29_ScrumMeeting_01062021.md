# Scrum Meeting 29
- Datum: 01.06.2021

## Was wurde gemacht?
### Pömmer:
- Mit Präsentation begonnen
- Bugfixing (Notification und TX Liste)
### Faltin:
- Ausblendung der Transaktionen und Notifikationen bei leerer Tabelle
- Styling der Single-Asset Page

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Präsensation
### Faltin:
- Präsensation
