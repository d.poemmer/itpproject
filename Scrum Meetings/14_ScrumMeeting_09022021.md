# Scrum Meeting 14
- Datum: 09.02.2021

## Was wurde gemacht?
### Pömmer:
- Tabelle für Transaktionen und Notifications pro Kryptowährung: Dynamisches einfügen und Event zum löschen der Einträge gemacht
### Faltin:
- Tabelle für Notifikationen und Transaktionen dynamisch laden

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Sprint 3 Abnahme Vorbereitung
- Sprint 4 Planung
- Anfangen Issue #11 Account: Account Page
### Faltin:
- Sprint 3 Abnahme Vorbereitung
- Sprint 4 Planung