# Scrum Meeting 21
- Datum: 06.04.2021

## Was wurde gemacht?
### Pömmer:
- Probleme mit prozentualer Veränderung gelöst (wurde falsch berechnet und falsch formatiert)
### Faltin:
- Bugs beheben fertigstellen

## Wo gabs Probleme?
### Pömmer:
- Anzeige der Prozentveränderung
### Faltin:
- kein

## Was wird gemacht?
### Pömmer:
- Anfangen mit der Funktion zur Berechnung des Portfoliocharts mit Berücksichtigung der Transaktionen
### Faltin:
- Timeframe Buttons (aktiven Button highlighten)
