# Scrum Meeting 09
- Datum: 15.12.2020

## Was wurde gemacht?
### Pömmer:
- Backend 
  - User Registrierung/Login Methoden programmiert
  - Datenbank zugriff möglich
### Faltin:
- Login-System implementieren

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Backend fertig stellen
- Funktionen der Test Page einbinden
- Sprint 2 vorbereiten
### Faltin:
- Frontent verbessern
- Login-System erweitern
- Protokoll für Sprintabnahme erstellen