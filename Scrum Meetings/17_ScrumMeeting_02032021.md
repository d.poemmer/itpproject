# Scrum Meeting 17
- Datum: 02.03.2021

## Was wurde gemacht?
### Pömmer:
- Probleme mit Repository gelöst
- Account Page: Username und Email dynamisch geladen
### Faltin:
- Fertigstellung des Pie-Charts

## Wo gabs Probleme?
### Pömmer:
- Repository (merge Konflikte, ...) -> gelöst
### Faltin:
- Darstellung des Charts

## Was wird gemacht?
### Pömmer:
- Account Page weiterarbeiten
### Faltin:
- Fehler bei Chart-Darstellung beheben 