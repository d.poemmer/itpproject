# Scrum Meeting 27
- Datum: 18.05.2021

## Was wurde gemacht?
### Pömmer:
- Daten Aktualisierungen bei AssetPage+Portfolio page behoben (+button)
- Bugfixing
- diverse Design Überarbeitungen
### Faltin:
- Ausblendung der Tabelle bei nicht vorhandenen Transaktinen und Notificaitons
- Versuch des Bugfixings beim Löschen der Transaktionen

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Frontend Design
- Testen (bugfinding + bugfixing)
### Faltin:
- Design-Verbesserungen im Frontend der Asset-Page
- Bug fixing bei Löschen der Transaktionen