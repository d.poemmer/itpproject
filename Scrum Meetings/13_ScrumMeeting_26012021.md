# Scrum Meeting 13
- Datum: 26.01.2021

## Was wurde gemacht?
### Pömmer:
- Confirm Dialog für Entfernen von Favoriten erstellt
- Notifications/Transactions Frontend gefixt
### Faltin:
- Backend für 
    - Notifications
    - Transaktionenen implementiert

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Tabelle für Transaktionen und Notifications pro Kryptowährung
### Faltin:
- Tabelle für Transaktionen und Notifications pro Kryptowährung