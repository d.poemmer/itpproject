# Scrum Meeting 25
- Datum: 04.05.2021

## Was wurde gemacht?
### Pömmer:
- Sprint 5 Abnahme Vorbereitung
- Pie Chart Background Color fixing
- CSS styling (chart Größe, consider-tx-button position)
### Faltin:
- Aktualisierung fertigstellen & Farbpaletten auswählen


## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Abnahme Sprint 5
- Planung des letzten Sprints
### Faltin:
- Abnahme Sprint 5
- Planung des letzten Sprints