# Scrum Meeting 16
- Datum: 23.02.2021

## Was wurde gemacht?
### Pömmer:
- Sprint 3 Abnahme Vorbereitung
- Sprint 4 Planung
- Angefangen mit Account Page

### Faltin:
- Erstellung des Portfolio-Pie-Chart

## Wo gabs Probleme?
### Pömmer:
- keine
### Faltin:
- keine

## Was wird gemacht?
### Pömmer:
- Account Page JS anfangen (Dynamisches Laden der Funktionen)
### Faltin:
- Fertigstellung des Pie-Charts