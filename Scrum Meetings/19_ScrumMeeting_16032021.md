# Scrum Meeting 19
- Datum: 16.03.2021

## Was wurde gemacht?
### Pömmer:
- Account Page Funktionen fertig gemacht
  - Changing Email
  - Changing Username
  - Changing Default Currency
### Faltin:
- Darstellung des Charts verfeinert und kleine Verbesserungen

## Wo gabs Probleme?
### Pömmer:
- kein
### Faltin:
- kein

## Was wird gemacht?
### Pömmer:
- Sprint 4 Abnahme
- Sprint 5 Planung
### Faltin:
- Sprint 4 Abnahme
- Sprint 5 Planung