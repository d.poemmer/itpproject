let PAGES;
let currentPage;
const DEFAULT_CURRENCY = 'usd';
let fiat = DEFAULT_CURRENCY;
let currencySigns = {
    "usd": "$",
    "eur": "€"
};
const tempimagePath = '../img/empty.png'
const coinsPerPage = 10;
let coinPagesShowing = 0;
let maxCoins = 100;
let portfolio = {};
let portfolioValue = 0;
let marketcoins = {};

$.when($.ready).then(function() {
    // init tablesort
    $('table').tablesort();

    // get pages
    PAGES = {
        home: $('#page-home'),
        account: $('#page-account'),
        accountLoggedOut: $('#page-account-loggedout'),
        portfolio: $('#page-portfolio'),
        singleasset: $('#page-singleasset')
    }

    // init home page
    showPage(PAGES.home);
    //setInterval(refreshMarketData,1000,5)

    // check if we are logged in
    token = getTokenFromCookie();
    
    // if there is an authentication token, we are logged in
    // console.log(token);
    if (token) {
        reqGetUser()
        .then(res => {
            // showPage(PAGES.account);

            console.log(`Logged in as ${res.name}:${res.email}`);
            toggleSignedInIcons(true, res.name);
            message(MESSAGES.loggedin); // show message that we are logged in
            
            loadMarketData(coinPagesShowing+1);
        })
        .catch(err => {
            console.log(err);
            clearAccessToken();
            toggleSignedInIcons(false, username);
        });
    }
    else {
        toggleSignedInIcons(false, username);
        
        loadMarketData(coinPagesShowing+1);
    }

    // showAssetPage('ripple');
});

$(window).scroll(function() {
    if ($(window).scrollTop() >= $(document).height() - $(window).height() - 100) {
        // console.log('bottom reached');
        if (currentPage == PAGES.home) {
            handleLoadNewMarketData();
        }
    }
    // if ($(window).scrollTop() + $(window).height() == $(document).height()) {
    //     // console.log('scolled to the bottom!');
    //     // if (currentPage == PAGES.home) {
    //     //     handleLoadNewMarketData();
    //     // }
    // }
    });

let alreadyLoadingMarketData = false;
function handleLoadNewMarketData() {
    if ((coinPagesShowing+1)*coinsPerPage > maxCoins) {
        console.log('already showing maximum amount of coins');
        return;
    }

    if (alreadyLoadingMarketData) {
        // console.log('already loading data...');
        return;
    }

    alreadyLoadingMarketData = true;
    loadMarketData(coinPagesShowing+1)
    .then((msg) => {
        // console.log(msg);
        alreadyLoadingMarketData = false;
    })
    .catch((err) => {
        console.log('error loading market data: ');
        console.log(err);
        alreadyLoadingMarketData = false;
    })
}


function loggedIn() {
    toggleSignedInIcons(true, username);
    showPage(PAGES.home);

    reqGetUser()
    .then(() => {
        updateMarketData();
    })
}

function logout() {
    clearAccessToken();
    toggleSignedInIcons(false);
    message(MESSAGES.loggedout); // show logged out message

    showPage(PAGES.home);
    navActiveButton('.btn-page-home');
    fiat = DEFAULT_CURRENCY;
    updateMarketData();
}