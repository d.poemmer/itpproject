let token = '';
let username = '';
let email = '';
let transactions = null;
let ip = 'localhost:8080';//'localhost:8080';//'192.168.43.34:8080';//'91.113.26.245:8080';

function formatErrorResponse(error) {
    console.log(error);
    let e = error.statusText;
    if (error.responseJSON) {
        e = error.responseJSON.message;
    }
    if (e == 'error') {
        e = 'Server not responding...'
    }
    return e;
}

function reqLogin(un, pw) {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/login';
        const Data = {
            username: un,
            password: pw
        };

        $.ajax({
            method: "POST",
            url: Url,
            timeout: 400,
            data: Data,
            success: function(res) {
                console.log(res.message);

                username = un;
                token = res.token;
                
                saveTokenAsCookie(token);
                loggedIn();
                resolve(res.message);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}

function reqRegister(un, em, pw) {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/register';
        const Data = {
            name: un,
            email: em,
            password: pw
        };

        $.ajax({
            method: "POST",
            url: Url,
            data: Data,
            success: function(res) {
                console.log(res.message);
                resolve(res.message);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}

function reqChangePassword(oldpw, newpw) {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/changePassword';
        const Data = {
            oldpw, newpw
        }

        $.ajax({
            method: 'POST',
            url: Url,
            data: Data,
            headers: {
                Authorization: 'Bearer ' + token
            },
            success: function(res) {
                resolve(res);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}
function reqChangeUsername(newName) {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/changeUsername';
        const Data = {
            newUsername: newName
        }

        $.ajax({
            method: 'POST',
            url: Url,
            data: Data,
            headers: {
                Authorization: 'Bearer ' + token
            },
            success: function(res) {
                resolve(res);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}
function reqChangeEmail(newEmail) {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/changeEmail';
        const Data = {
            newEmail
        }

        $.ajax({
            method: 'POST',
            url: Url,
            data: Data,
            headers: {
                Authorization: 'Bearer ' + token
            },
            success: function(res) {
                resolve(res);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}

function reqChangeCurrency(newCurr) {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/changeCurrency';
        const Data = {
            newCurrency: newCurr
        }

        $.ajax({
            method: 'POST',
            url: Url,
            data: Data,
            headers: {
                Authorization: 'Bearer ' + token
            },
            success: function(res) {
                resolve(res);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}


function reqGetUser() {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/getUser';

        $.ajax({
            method: "GET",
            url: Url,
            headers: {
                Authorization: "Bearer " + token
            },
            success: function(res) {
                username = res.name;
                email = res.email;
                fiat = res.fiat
                resolve(res);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}

function reqDeleteAccount() {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/deleteUser';

        $.ajax({
            method: 'GET',
            url: Url,
            headers: {
                Authorization: 'Bearer ' + token
            },
            success: function(res) {
                resolve(res);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}

function reqGetPortfolio() {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/getPortfolio';

        $.ajax({
            method: "GET",
            url: Url,
            headers: {
                Authorization: "Bearer " + token
            },
            success: function(res) {
                resolve(res.portfolio);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}

function reqGetTransactions() {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/getTransactions';

        $.ajax({
            method: "GET",
            url: Url,
            headers: {
                Authorization: "Bearer " + token
            },
            success: function(res) {
                resolve(res.transactions);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}
function reqAddTransaction(tx) {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/addTransaction';
        const Data = {
            transaction: tx
        }

        $.ajax({
            method: "POST",
            url: Url,
            data: Data,
            headers: {
                Authorization: "Bearer " + token
            },
            success: function(res) {
                resolve(res.message);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}
function reqRemoveTransaction(id) {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/removeTransaction';
        const Data = {
            transactionId: id
        }

        $.ajax({
            method: "POST",
            url: Url,
            data: Data,
            headers: {
                Authorization: "Bearer " + token
            },
            success: function(res) {
                resolve(res.message);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}

// function reqUpdateTransactions(tx) {
//     // return new Promise(function(resolve, reject) {
//         const Url = 'http://'+ip+'/api/user/updateTransactions';
//         const Data = {
//             transactions: tx
//         }

//         $.ajax({
//             method: "POST",
//             url: Url,
//             data: Data,
//             headers: {
//                 Authorization: "Bearer " + token
//             },
//             success: function(res) {
//                 console.log(res.message);
//             },
//             error: function(error) {
//                 reject(new Error(formatErrorResponse(error)));
//             }
//         })
//     // })
// }

function reqGetNotifications() {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/getNotifications';

        $.ajax({
            method: "GET",
            url: Url,
            headers: {
                Authorization: "Bearer " + token
            },
            success: function(res) {
                resolve(res.notifications);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}

function reqAddNotification(n) {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/addNotification';
        const Data = {
            notification: n
        }

        $.ajax({
            method: "POST",
            url: Url,
            data: Data,
            headers: {
                Authorization: "Bearer " + token
            },
            success: function(res) {
                resolve(res.message);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}
function reqRemoveNotification(id) {
    return new Promise(function(resolve, reject) {
        const Url = 'http://'+ip+'/api/user/removeNotification';
        const Data = {
            notificationId: id
        }

        $.ajax({
            method: "POST",
            url: Url,
            data: Data,
            headers: {
                Authorization: "Bearer " + token
            },
            success: function(res) {
                resolve(res.message);
            },
            error: function(error) {
                reject(new Error(formatErrorResponse(error)));
            }
        })
    })
}

// function reqUpdateNotifications(n) {
//     // return new Promise(function(resolve, reject) {
//         const Url = 'http://'+ip+'/api/user/updateNotifications';
//         const Data = {
//             notifications: n
//         }

//         $.ajax({
//             method: "POST",
//             url: Url,
//             data: Data,
//             headers: {
//                 Authorization: "Bearer " + token
//             },
//             success: function(res) {
//                 console.log(res.message);
//             },
//             error: function(error) {
//                 reject(new Error(formatErrorResponse(error)));
//             }
//         })
//     // })
// }