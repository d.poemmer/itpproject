let chartBackgroundColor    = '#51525122';//'#dcfbef';
let chartBorderColor        = '#40403f';//'#11d171';

let staticColors = [
    // GB Palette
    // 'rgba(0, 168, 255,1.0)',
    // 'rgba(156, 136, 255,1.0)',
    // 'rgba(251, 197, 49,1.0)',
    // 'rgba(72, 126, 176,1.0)',
    // 'rgba(232, 65, 24,1.0)',
    // 'rgba(245, 246, 250,1.0)',
    // 'rgba(127, 143, 166,1.0)',
    // 'rgba(39, 60, 117,1.0)',
    // 'rgba(53, 59, 72,1.0)'

    // DE Palette
    'rgba(252, 92, 101,1.0)',
    'rgba(253, 150, 68,1.0)',
    'rgba(254, 211, 48,1.0)',
    'rgba(38, 222, 129,1.0)',
    'rgba(43, 203, 186,1.0)',
    'rgba(69, 170, 242,1.0)',
    'rgba(75, 123, 236,1.0)',
    'rgba(165, 94, 234,1.0)',
    'rgba(209, 216, 224,1.0)',
    'rgba(119, 140, 163,1.0)'
]

async function getChartDataOnTransactions(timeframe, transactions) {
    // console.warn('transactions chart data: ');
    // console.log(transactions);

    // 1) get chart data for every coin in transactions
    let coindata = {}
    let timestamps = []
    for (const id in transactions) {
        if (!coindata[id]) {
            await getChartData(id, timeframe)
            .then(data => {
                // console.log('data of: ' + id + ' in a timeframe of: ' + timeframe);
                // console.log(data);
                
                if (timestamps.length == 0) {
                    timestamps = data[0];
                }

                coindata[id] = {
                    timestamps: data[0],    // get timestamps
                    values: data[1]         // get values
                }
            });
        }
    }

    // console.log('Coindata:');
    // console.log(coindata);

    // 2) get txValues 
    // tx = multiply transaction amount with the chart data
    let data = [] // [0]: transactions; [1]: values
    data[0] = timestamps // timestamps
    data[1] = [] // values

    for (const coinId in transactions) {
        // console.log('tx values of: ' + coinId);
        let txValues = []
        for (const coinTxIdx in transactions[coinId]) {
            for (const valueIdx in coindata[coinId].values) {
                // multiply with 0 if the timestamp is below the transaction time
                if (new Date(coindata[coinId].timestamps[valueIdx]) > new Date(transactions[coinId][coinTxIdx].time)) {
                    // calculate the value at that time of that current transaction
                    let v = (coindata[coinId].values[valueIdx]*transactions[coinId][coinTxIdx].amount)
                    if (txValues[valueIdx] == 0 || !txValues[valueIdx]) {
                        txValues[valueIdx] = (v)
                    } else {
                        txValues[valueIdx] += v;
                    }
                } else {
                    if (!txValues[valueIdx]) {
                        txValues[valueIdx] = 0;
                    }
                }
            }
        }
        // console.log(txValues);

        // 3) calculate get sum of all tx
        for (const valueIdx in txValues) {
            if (data[1][valueIdx]) {
                data[1][valueIdx] += txValues[valueIdx]
            } else {
                data[1][valueIdx] = txValues[valueIdx]
            }
        }
    }
    // console.log('data');
    // console.log(data);
    
    return data
}

function getChartData(coin, timeframe) {
    return new Promise ((resolve, reject) => {
        if (!coin || !timeframe) {
            console.log('get chart data:');
            console.log('coin: ' + coin);
            console.log('timeframe: ' + timeframe);
            reject('no coin or timeframe specified');
            return;
        }

        // calculate time days back for the desired timeframe
        let s = timeframe.substr(timeframe.length-1, 1);
        let span = parseInt(timeframe.substr(0, timeframe.length-1));

        let daysCount = span;
        // get number of days back
        if (s == 'h')
            daysCount = 1;
        else if (s == 'w')
            daysCount = span*7;
        else if (s == 'm')
            daysCount = span*12;
        else if (s == 'y')
            daysCount = span*365;
        
        let interval = 'daily';
        // get the interval
        if (daysCount < 22)
            interval = 'hourly';
        if (daysCount >= 365*2)
            interval = 'monthly';

        // call the api
        let callURL = "https://api.coingecko.com/api/v3/coins/"+coin+"/market_chart?vs_currency="+fiat+"&days="+daysCount+"+&interval="+interval;
        // let callURL = "https://api.coingecko.com/api/v3/coins/"+coin+"/market_chart/range?vs_currency=usd&from="+fromDate+"&to="+toDate;
        
        $.getJSON(callURL, apicallready)
        .fail(function() {
            console.log(callURL + '; might not be available');
            reject('error with calling the api');
        });

        function apicallready(data) {
            let chartData = data["prices"];

            chartData = limitDataPoints(chartData, 24)

            chartData = formatChartData(chartData);

            resolve(chartData);
        }
    });
}
function formatChartData(chartData) {
    let timestamps = [];
    chartData.forEach(element => timestamps.push(getDateFormatted(element[0])));
    let prices = [];
    chartData.forEach(element => prices.push(element[1]));//Math.round(parseFloat(element[1])*10000)/10000));
    return [timestamps, prices];
}

function getMarketCoins(amount, page) {
    return new Promise ((resolve, reject) => {
        let callURL = "https://api.coingecko.com/api/v3/coins/markets?vs_currency="+fiat+"&order=market_cap_desc&per_page="+amount+"&page="+page+"&sparkline=false&price_change_percentage=24h"; // get from api

        $.getJSON(callURL, apicallready)
        .fail(function() {
            console.log(callURL + '; might not be available');
            reject('error with calling the api');
        });

        function apicallready(data) {
            let coinsFormatted = {};

            for (entry in data) {
                let coin = data[entry];
                coinsFormatted[coin.id] = formatCoin(coin);
            }
            
            resolve(coinsFormatted);
        }
    });
}
function formatCoin(coin) {
    return {
        "id": coin.id,
        "name": coin.name,
        "img": coin.image,
        "rank": coin.market_cap_rank,
        "price": coin.current_price,
        "percChange": coin.price_change_percentage_24h,
        "priceChange": coin.price_change_24h,
        "marketCap": coin.market_cap,
        "volume": coin.total_volume
        // , ... (can get more if needed)
    };
}


function beforePrintHandler() {
    for (var id in Chart.instances) {
        Chart.instances[id].resize();
    }
}
// TODO: not called :(
// window.addEventListener("beforeprint", function(event) { 
//     console.log('print1');
//     beforePrintHandler();
// });
let currentChart = null;
function displayChart(chartID, timestamps, prices, fiat) {
    let ctx = $(chartID);
    if (currentChart != null) {
        currentChart.destroy();
    }
    currentChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: timestamps,
            datasets: [{
                label: 'not shown',
                data: prices,
                backgroundColor: [
                    chartBackgroundColor,
                ],
                borderColor: function(context) {
                    return chartBorderColor;
                    // let index = context.dataIndex;
                    // let value = context.dataset.data[index];
                    // return index % 2 ? 'yellow' : 'black';
                },
                borderWidth: 1
            }]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            legend: {
                display: false
            },
            layout: {
                padding: 25 // {left: 50, right: 0, top: 0, bottom: 0}
            },
            elements: {
                point: {
                    radius: 0
                }
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                custom: function(tooltip) {
                    if (!tooltip) return;
                    tooltip.displayColors = false;
                },
                callbacks: {
                    label: function(tooltipItem, data) {
                        // var label = data.datasets[tooltipItem.datasetIndex].label || '';
    
                        // if (label) {
                        //     label += ': ';
                        // }
                        let label = tooltipItem.xLabel;
                        let value = tooltipItem.yLabel;
                        if (value < 1) {
                            value = (Math.floor(tooltipItem.yLabel*10000)/10000)
                        }
                        else if (value < 100) {
                            value = (Math.floor(tooltipItem.yLabel*100)/100);
                        }
                        else {
                            value = Math.floor(value);
                        }
                        label += ': ' + value + ' ' + currencySigns[fiat];
                        return label;
                    },
                    title: function(tooltipItem, data) {
                        return;
                    }
                }
            },
            scales: {
                xAxes: [{
                    // TODO: https://www.chartjs.org/docs/latest/axes/cartesian/time.html
                    // type: 'time',
                    // // distribution: 'series',
                    
                    // time: {
                    //     //format: 'YYYY.MM.DD, hh:mm',
                    //     //tooltipFormat: 'll'
                    //     unit: 'hour',
                    //     // displayFormats: {
                    //         // quarter: 'MMM YYYY'
                    //         // second: 'h:mm:ss a'
                    //     // }
                    // }
                    type: 'time',
                    time: {
                        // tooltipFormat: 'll',
                        // unit: 'hour'
                    },
                    gridLines: {
                        drawOnChartArea: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        callback: function(label, index, labels) {
                            return formatPrice(label) + ' ' + currencySigns[fiat];
                        }
                    },
                    // type: 'logarithmic',
                    gridLines: {
                        drawOnChartArea: false
                    }
                }]
            }
        }
    });

    // currentChart.canvas.style.height = '500px';
    // currentChart.canvas.parentNode.style.width = '100%';
    // currentChart.canvas.parentNode.style.height = '100%';
    // console.log("portfoliochart");

}
function poolColors(amount) {
    var pool = [];

    // randomize colors
    for (let i = 0; i < staticColors.length; i++) {
        let randomIndex = Math.floor(Math.random()*staticColors.length);
        let temp = staticColors[randomIndex];
        staticColors[randomIndex] = staticColors[i];
        staticColors[i] = temp;
    }

    // get colors
    let size = amount > staticColors.length ? staticColors.length : amount;
    for(i = 0; i < size; i++) {
        pool.push(staticColors[i]);
    }

    return pool;
}

let currentPieChart = null;
function displayPieChart(chartID,values,coins) {
    let ctxP = $(chartID);
    // console.log(coins);
    if (currentPieChart != null) {
        currentPieChart.destroy();
    }
    let portfolioValue = 0;
    for (let v of values) {
        portfolioValue += v;
    }

    currentPieChart = new Chart(ctxP, {
        type: 'doughnut',
        data: {
            labels: coins,
            label: 'not shown',
            datasets: [{
                data: values,
                backgroundColor: poolColors(coins.length),
                borderWidth: 0
            }]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        // var label = data.datasets[tooltipItem.datasetIndex].label || '';
    
                        // if (label) {
                        //     label += ': ';
                        // }
                        // label += Math.round(tooltipItem.yLabel * 100) / 100;
                        // return label;

                        let label = data.labels[tooltipItem.index];
                        let price = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        return label + ': ' + formatPrice(price) + currencySigns[fiat] + ' (' + formatPerc((100*price)/portfolioValue) + '%)';
                    }
                }
            }
        }
        
    });
    // console.log("piechart");
};


function apiCoinInformations(coin, fiat) {
    return new Promise (function(resolve, reject) {
        // let callURL = "https://api.coingecko.com/api/v3/simple/price?ids="+coin+"&vs_currencies="+fiat;
        let callURL = 'https://api.coingecko.com/api/v3/coins/markets?vs_currency='+fiat+'&ids='+coin+'&order=market_cap_desc&per_page=100&page=1&sparkline=false';
        let responseObject = $.getJSON(callURL, apicallready);
        
        function apicallready(data) {
            data[0] ? resolve(data[0]) : resolve(0);
        }
    });
}

function apiSinglePrice(coin, fiat) {
    return new Promise (function(resolve, reject) {
        // if (coin != undefined) {
        //     console.log('apiSinglePrice: no coin given!');
        //     reject('apiSinglePrice: no coin given!');
        // }
        // if (fiat != undefined) {
        //     console.log('apiSinglePrice: no currency given!');
        //     reject('apiSinglePrice: no currency given!');
        // }

        apiCoinInformations(coin, fiat)
        .then(data => {
            resolve(data['current_price']);
        });
    });
}

function limitDataPoints(data, limit) {
    let newData = [];
    let size = data.length;

    if (size > limit) {
        let index = size-1;
        let n = size/limit;
        for (let i = limit-1; i >= 0; i--) {
            newData.unshift(data[Math.floor(index)]);
            index -= n;
        }
    }
    else {
        newData = data;
    }

    return newData;
}