const CHART_REFRESH_TIME = 30000;
const MARKET_DATA_REFRESH_TIME = 60000;
const ASSET_REFRESH_TIME = 30000;
const PORTFOLIO_REFRESH_TIME = 120000;
const DEBUG_LOADER = false;
let currentTimeframe = '24h';
let considerTx = true;

function saveTokenAsCookie(token) {
    let expireDate = new Date();
    expireDate.setHours(expireDate.getHours() + 12);

    document.cookie = 'access_token='+token+"; " + expireDate;
}

function getTokenFromCookie() {
    let cookiesString = document.cookie;
    let cookies = cookiesString.split(';');
    let token;

    for (c of cookies) {
        if (c.includes('access_token=')) {
            let a = c.replace('access_token=', '').replace(' ', '');
            token = a; // c.slice(13);
            break;
        }
    }
    // console.log(token)

    return token;
}
function clearAccessToken() {
    document.cookie = 'access_token=';
    token = '';
}

function toggleConsiderTransactions(isActive) {
    if (considerTx != isActive) {
        considerTx = isActive;
        if (currentPage == PAGES.portfolio) {
            refreshPortfolioChart(currentTimeframe);
        }
    }
}

function refreshPortfolioChart(t = '24h') {
    currentTimeframe = t;
    if (considerTx) {
        displayPortfolioChartOnTransactions(t);
    } else {
        displayPortfolioChart(t);
    }
}

function showPortfolioPage() {
    if (currentPage == PAGES.portfolio) {
        return;
    }

    // let loggedin = token ? true : false;
    // togglePortfolioView(loggedin);

    showPage(PAGES.portfolio);

    // if (loggedin) {
    setTimeout(() => {

    reqGetPortfolio()
    .then(p => {
        portfolio = p;

        // add all portfolio entries to the tbody
        let tbody = $('#portfolio-coins');
        tbody.empty();

        // reset current timeframe
        currentTimeframe = '24h';
        refreshPortfolioPage();

        let interval = setInterval(() => {
            if (currentPage != PAGES.portfolio) {
                clearInterval(interval);
                return;
            }
            refreshPortfolioPage();
        }, PORTFOLIO_REFRESH_TIME)
    })
    .catch(error => {
        console.log(error);
        message(MESSAGES.generic, 'Portfolio could not be loaded!');
    });

    }, DEBUG_LOADER ? 50000 : 0);

    // }
}
async function setPortfolioValue() {
    portfolioValue = 0;

    $('#portfolio-coins').empty();

    for (i in portfolio) {
        let id = i;
        let coin;
        if (marketcoins[id] != undefined) {
            coin = marketcoins[id];
            appendPortfolioEntry(coin, portfolio[id]);
            portfolioValue += coin.price*portfolio[id];
            // console.log('portfolio value from given');
            // console.log(coin+" "+coin.price);
        } else {
            // await is not really working
            await apiCoinInformations(id, fiat) //vielleicht Fehler!!!
            .then(c => {
                coiner = formatCoin(c);
                appendPortfolioEntry(coiner, portfolio[id]);
                portfolioValue += coiner.price*portfolio[id];
                // console.log('portfolio value new price:'+portfolioValue);
            });
        }
    }

    // upddate portfolio value
    $('#page-portfolio .portfolio-value').text(formatPrice(portfolioValue)+currencySigns[fiat]);
}
async function displayPortfolioChartOnTransactions(timeframe = '24h') {

    // get portfolio chartdata
    if (!transactions) {
        await reqGetTransactions()
        .then(t => {
            transactions = t
        })
    }

    let data = [[new Date()], [0]];

    if (Object.keys(transactions).length == 0) {
        message(MESSAGES.generic, 'Your portfolio is empty. Add Coins to your portfolio by adding transactions to the coins.');
    }
    else {
        data = await getChartDataOnTransactions(timeframe, transactions)
    }

    let timestamps = data[0]
    let values = data[1]

    if (!values || !timestamps) {
        console.log('error: getChartDataOnTransactions returns no timestamps/values');
        console.log(data);
    }

    // update Price Change Percentage
    let percentageChange = 0;
    if (values.length > 1) {
        percentageChange = 100*(values[values.length-1]-values[0])/values[0];
    }
    updatePortfolioPriceChange(percentageChange);
    
    // display the portfolio chart
    displayChart('#portfolio-chart', timestamps, values, fiat);

}
async function displayPortfolioChart(timeframe = '24h') {
    // get portfolio chartdata
    let values = [];
    let timestamps = [];
    for (id in portfolio) {
        let holdings = portfolio[id];
        await getChartData(id, timeframe)
        .then(data => {
            //console.log('Test');

            // get timestamps
            if (timestamps.length == 0) {
                timestamps = data[0];
            }
            // init prices with first coin
            if (values.length == 0) {
                for (idx in data[1]) {
                    let price = data[1][idx];
                    values.push(price*holdings);
                }
            }
            // add value to current
            else {
                for (let i = 0; i < data[1].length; i++) {
                    values[i] += data[1][i]*holdings;
                }
            }
        });
    }
    // update Price Change Percentage
    let p = 100*(values[values.length-1]-values[0])/values[0];
    updatePortfolioPriceChange(p);
    
    displayChart('#portfolio-chart', timestamps, values, fiat);
}
function updatePortfolioPriceChange(p) {
    let priceChangePerc = formatPerc(p);

    if (priceChangePerc > 0) {
        $('.po-informations .cont-percentage').css('color', '#2c662d');
    } else {
        $('.po-informations .cont-percentage').css('color', '#9f3a38');
    }
    $('.po-changeperc').text(priceChangePerc+'%');
}

async function displayPieChartData() {
    var values = [];
    var prices =[];
    var holdings = [];
    // var coins = [];
    for (id in portfolio) {
        holdings.push(portfolio[id]);
        await apiSinglePrice(id, fiat)
        .then(p => {
            prices.push(p);
        });
        
        /*for(i=0;i<Object.keys(portfolio).length;i++){
            values[i]=prices[i]*holdings[i];
            console.log(prices[i]);
            console.log(holdings[i]);
        }*/

        // values.push(prices*holdings);
       
        // coins.push(id);
    }
    for(i=0;i<Object.keys(portfolio).length;i++){
        values[i]=prices[i]*holdings[i];
    }
    // console.log(values);
    // console.log(portfolio);
    displayPieChart('#pieC',values,portfolioWithoutFav(portfolio));
    
}
function portfolioWithoutFav(obj){
    for(var propName in obj)
    {
        if(obj[propName]==0){
            delete obj[propName];
        }

    }
    return Object.keys(obj);
}
function appendPortfolioEntry(coin, holdings) {
    let tbody = $('#portfolio-coins');
    apiSinglePrice(coin,fiat);
    // console.log(fiat+" fiat");
    let posneg = 'negative';
    if (coin.priceChange > 0) {
        posneg = 'positive';
    }

    // let tr = $(
    //     '<tr class="market-info-row">'+
    //         '<td>'+coin.rank+'</td>'+
    //         nameTag +
    //         '<td>'+coin.price+currencySigns[fiat]+'</td>'+
    //         '<td class="'+posneg+'">'+coin.percChange+'%</td>'+
    //         '<td class="'+posneg+'">'+coin.priceChange+currencySigns[fiat]+'</td>'+
    //         '<td>'+coin.marketCap+currencySigns[fiat]+'</td>'+
    //         '<td>'+coin.volume+currencySigns[fiat]+'</td>'+
    //     '</tr>');
    let tr = $(
        `<tr class="portfolio-coin-row">
            <td class="coinname">
                <img class="market-info-img" src="${coin.img}">
                <h2>${coin.name}</h2>
            </td>
            <td class="${posneg}">
                <div class="price">
                    <h3>${formatPrice(coin.price)}${currencySigns[fiat]}</h3>
                    <p>${formatPerc(coin.percChange)}%</p>
                </div>
            </td>
            <td>
                <div class="holdings">
                    <h3>${formatPrice(holdings*coin.price)}${currencySigns[fiat]}</h3>
                    <p>${holdings}</p>
                </div>
            </td>
            <input type="hidden" value="${coin.id}">
        </tr>`);
    $(tr).on('click', function() {
        let id = this.children[this.childElementCount-1].value;
        showAssetPage(id);
    });
    tbody.append(
        tr
    );
}
function togglePortfolioView(isLoggedIn) {
    $('#page-portfolio-loggedout').toggle(!isLoggedIn);
    $('#page-portfolio').toggle(isLoggedIn);
}

function addNotification(id, price) {
    reqAddNotification({id, price})
    .then(msg => {
        updateNotificationList(id);
        console.log(msg);
    })
    .catch(err => {
        console.log(err);
    })
}
function addTransaction(id, a, date) {
    date = new Date(date);
    
    reqAddTransaction({id: id, time: date, amount: a})
    .then(msg => {
        updateTransactionList(id);
        updateFavoriteButton(id);
        console.log(msg);
    })
    .catch(err => {
        console.log(err);
    })
}

function updateAccountPage() {
    // check if logged in
    if (isLoggedIn()) {
        console.log('logged in')
        reqGetUser()
        .then(u => {
            username = u.name;
            email = u.email;
            fiat = u.fiat;
            fillInAccountDetails();
        })
        
    } else {
        console.log('not logged in')
        // showPage(PAGES.accountLoggedOut);
    }
}
function fillInAccountDetails() {
    $('#page-account .varusername').text(username);
    $('#page-account .varemail').text(email);
    $('.currencydropdown').dropdown('set selected', fiat);
}

let assetInterval = null;
let chartInterval = null;
function showAssetPage(name) {
    if (!name) {
        console.log('cannot show asset page: no asset name specified');
        return;
    }
    // show the page
    showPage(PAGES.singleasset);

    // default favorite button to not favorite
    $('.btn-favorite i').addClass('outline');

    $('.sa-informations .cont-percentage').css('color', '#000000');

    if (isLoggedIn()) {
        $('#page-singleasset .tableWrapper').show();

        // get transactions
        updateTransactionList(name);
        
        // get notifications
        updateNotificationList(name);
    } else {
        $('#page-singleasset .tableWrapper').hide();
    }

    setTimeout(() => {
    
    // get Informations about the coin
    apiCoinInformations(name, fiat)
    .then(data => {
        let imgsrc = data.image;
        let coinname = data.name;
        let price = formatPrice(data.current_price);
        let marketcap = formatMarketCap(data.market_cap);
        // let rank = data.market_cap_rank;
        // let priceChange = data.price_change_24h;
        
        //updateSingleAssetPriceChange(data.price_change_percentage_24h);


        // put the information into the html elements
        $('.btn-asset-refresh').attr('value', name);
        $('.sa-id').attr('value', name);
        $('.sa-img').attr('src', imgsrc);
        $('.sa-header').text(coinname);
        $('.sa-price').text(price+currencySigns[fiat]);
        $('.sa-market').text(marketcap+currencySigns[fiat]);
        // $('.sa-rank').text(rank);
        // $('.sa-change').text(priceChange);

        // show the chart
        displaySingleAssetChart(name, currentTimeframe);

        // get portfolio if logged in
        let loggedin = token ? true : false;
        if (Object.keys(portfolio).length <= 0 && loggedin) {
            reqGetPortfolio()
            .then(p => {
                portfolio = p;
                
                initFavoriteButton(name);
            });
        }
        else {
            initFavoriteButton(name);
        }

    });

    if (assetInterval) {
        clearInterval(assetInterval);
    }
    assetInterval = setInterval(() => {
        if (currentPage != PAGES.singleasset) {
            clearInterval(assetInterval)
            return;
        }
        console.log('refreshing asset page');
        refreshAssetPageInformations(name)
    }, ASSET_REFRESH_TIME);

    if (chartInterval) {
        clearInterval(chartInterval);
    }
    chartInterval = setInterval(() => {
        if (currentPage != PAGES.singleasset) {
            clearInterval(chartInterval)
            return;
        }
        console.log('refreshing chart');
        displaySingleAssetChart(name, currentTimeframe);
    }, CHART_REFRESH_TIME);

    }, DEBUG_LOADER ? 5000 : 0);

}
function refreshAssetPage(name) {
    console.log('refreshing asset page: ' + name);
    refreshAssetPageInformations(name);
    displaySingleAssetChart(name, currentTimeframe);
}
function refreshPortfolioPage() {
    setPortfolioValue();
    
    refreshPortfolioChart(currentTimeframe)

    displayPieChartData();
}
function refreshAssetPageInformations(name) {
    // console.log(name);
    // console.log(currentTimeframe);

    // get Informations about the coin
    apiCoinInformations(name, fiat)
    .then(data => {
        let imgsrc = data.image;
        let coinname = data.name;
        let price = formatPrice(data.current_price);
        let marketcap = formatMarketCap(data.market_cap);
        // let rank = data.market_cap_rank;
        // let priceChange = data.price_change_24h;

        // put the information into the html elements
        $('.sa-id').attr('value', name);
        $('.sa-img').attr('src', imgsrc);
        $('.sa-header').text(coinname);
        $('.sa-price').text(price+currencySigns[fiat]);
        $('.sa-market').text(marketcap+currencySigns[fiat]);
    });
}

function updateSingleAssetPriceChange(p) {
    let priceChangePerc = formatPerc(p);

    if (priceChangePerc > 0) {
        $('.sa-informations .cont-percentage').css('color', '#2c662d');
    } else {
        $('.sa-informations .cont-percentage').css('color', '#9f3a38');
    }
    $('.sa-changeperc').text(priceChangePerc+'%');
}
function updateTransactionList(name) {
    reqGetTransactions()
    .then(t => {
        transactions = t

        // hide the transaction table if there are no transactions
        if (transactions && transactions[name]) {
            if (Object.keys(transactions[name]).length == 0) {
                $('.transactiontable').hide();
            }
            else {
                $('.transactiontable').show();
            }
        } else {
            $('.transactiontable').hide();
            return;
        }


        let tbody = $('.transactiontable tbody');
        tbody.empty();

        let tCoin = t[name];
        tCoin = tCoin.sort((a, b) => new Date(b.time) - new Date(a.time));

        for (idx in tCoin) {
            let dateFormatted = getDateFormattedUserFriendly(tCoin[idx].time);
            let date = new Date(tCoin[idx].time);
            if (tCoin[idx].amount != 0) {
                let removeTXButton = $(`<button class="ui icon button removeTransactionButton"><i class="trash icon"></i></button>`);
                let editTXButton = $(`<button class="ui icon button editTransactionButton"><i class="edit icon"></i></button>`);

                let td = $('<td></td>');
                td.append(removeTXButton);
                td.append(editTXButton);
                // console.log(td.html());

                let posneg = tCoin[idx].amount < 0 ? "negative" : "positive";
                let side = posneg == "positive" ? "buy" : "sell";

                let tr = $(`
                <tr>
                    <td>
                        <h4 class="ui header">
                            <div class="content">${dateFormatted}</div>
                        </h4>
                    </td>
                    <td class=${posneg}>
                        ${side}
                    </td>
                    <td>
                        ${Math.abs(tCoin[idx].amount)}
                    </td>
                </tr>
                `)

                tr.append(td);
                tr.append($(
                    `<input type="hidden" value="${tCoin[idx].id}">`
                ));

                tbody.append(tr);

                // tbody.append(
                // `
                // <tr>
                //     <td>
                //         <h4 class="ui header">
                //             <div class="content">${date}</div>
                //         </h4>
                //     </td>
                //     <td>
                //         ${tCoin[idx].amount}
                //     </td>
                //     <input type="hidden" value="${tCoin[idx].id}">
                // </tr>
                // `
                // );

                // TODO: remove removing whole table
                // Init Remove Transaction Button
                let txId = tCoin[idx].id;
                $(removeTXButton).on('click', function() {
                    console.log('remove tx: ');
                    console.log(txId);
                    reqRemoveTransaction(txId)
                    .then(msg => {
                        updateTransactionList(name);
                        updateFavoriteButton(name);
                    })
                    .catch(err => {

                    });
                });
                let amount = tCoin[idx].amount;
                // Init Edit Transaction Button
                $(editTXButton).on('click', function() {
                    //this could be a function in semanticInit.js
                    $('#modal-transaction').modal('show');
                    $('#modal-transaction .input-amount').val(amount);
                    let isoString = date.toISOString();
                    let d = isoString.substring(0, (isoString.indexOf("T")|0) + 6|0);
                    $('#modal-transaction .input-date').value = d;
                    // $('#modal-transaction .input-date').val(new Date());  //.toISOString().slice(0, 10));
                    //very bad -> update instead
                    // let id = $(this).parent().next().val();
                    reqRemoveTransaction(txId);
                });
            }
        }
        // // Init Remove Transaction Button
        // $('.removeTransactionButton').on('click', function() {
        //     let id = $(this).parent().next().val();
        //     /*reqRemoveTransaction(id)
        //     .then(msg => {
        //         updateTransactionList(id);
        //         //updateFavoriteButton(id);
        //     })
        //     .catch(err => {

        //     });
        //     */
        // });
        // $('.editTransactionButton').on('click', function() {
        //     //this could be a function in semanticInit.js
        //     $('#modal-transaction').modal('show');
        //     $('#modal-transaction .input-amount').val(a);
        //     $('#modal-transaction .input-date').val(d.toISOString().slice(0, 10));
        //     //very bad -> update instead
        //     let id = $(this).parent().next().val();
        //     reqRemoveTransaction(id);
        // });
    })
}
function updateNotificationList(name) {
    reqGetNotifications()
    .then(n => {
        // if (!n) n = {};
        // n[id] = price;
        // reqUpdateNotifications(n);

        if (n && n[name]) {
            if (Object.keys(n[name]).length == 0) {
                $('.notificationtable').hide();
            }
            else {
                $('.notificationtable').show();
            }
        } else {
            $('.notificationtable').hide();
        }

        let tbody = $('.notificationtable tbody');
        tbody.empty();
        let nCoin = n[name];
        for (idx in nCoin) {
            let price = formatPrice(nCoin[idx].price) + currencySigns[fiat];
            tbody.append(
            `<tr>
                <td>
                    <h4 class="ui header">
                        <div class="content">${price}</div>
                    </h4>
                </td>
                <td>
                    <button class="ui icon button removeNotificationButton"><i class="trash icon"></i></button>
                    <button class="ui icon button editNotificationButton"><i class="edit icon"></i></button>
                </td>
                <input type="hidden" value="${nCoin[idx].id}">
            </tr>`
            );
        }
        // Init Remove Transaction Button
        $('.removeNotificationButton').on('click', function() {
            let id = $(this).parent().next().val();
            console.log('remove noti: ');
            console.log(id);
            reqRemoveNotification(id)
            .then(msg => {
                updateNotificationList(name)
            })
            .catch(err => {
                message(MESSAGES.error, err);
            });
        });
        $('.editNotificationButton').on('click', function() {
            console.log('edit not: ');
            $('#modal-notification').modal('show');
            let price = $(this).parent().parent().find('.content').text();
            $('#modal-notification .input-price').val(price);
            let id = $(this).parent().next().val();
            reqRemoveNotification(id);
        });
    });
}

// TODO: rename
function displaySingleAssetChart(name, timeframe='24h') {
    currentTimeframe = timeframe;
    // TODO: 
    // - add data points to the chart depending on timeframe
    // - clear interval when:
    //  - this method is called again
    //  - currentPage changes from PAGES.singleAsset to different page 

    // $(document).on('currentPageChanged', function(newPage) {
    //     console.log('page changed');
    // })

    getChartData(name, timeframe)
    .then(coindata => {

        let timestamps  = coindata[0];
        let prices      = coindata[1];

        // update Price Change
        let p = 100*(prices[prices.length-1]-prices[0])/prices[0];
        updateSingleAssetPriceChange(p);

        displayChart('#sa-chart', timestamps, prices, fiat);

        // // show the page
        // showPage(PAGES.singleasset);
    })
    .catch(error => {
        console.log(error)
    });
}

function updateFavoriteButton(name) {
    // console.log(portfolio);
    // console.log(Object.keys(portfolio).length);
    if (Object.keys(portfolio).length > 0) {
        if (name in portfolio) {
            console.log('is favorite');
            $('.btn-favorite i').removeClass('outline')
            return true;
        }
    }

    console.log('is not favorite');
    $('.btn-favorite i').addClass('outline')
    return false;
}
function initFavoriteButton(name) {
    // check if favorite
    let isFavorite = false;

    isFavorite = updateFavoriteButton(name);

    // favorite button event
    $('.btn-favorite').on('click', function() {
        // check if logged in
        let loggedin = token ? true : false;
        if (!loggedin) {
            // Display Warning Message
            message(MESSAGES.generic, "Log in to use this feature!");
            return;
        }

        // update favorite status
        isFavorite = updateFavoriteButton(name)

        // check if allready favorite
        if (!isFavorite) {
            // make favorite
            $('.btn-favorite i').removeClass('outline');
            isFavorite = true;

            let currentTime = new Date();
            reqGetTransactions()
            .then(t => {
                let newT = t;
                console.log(currentTime);
                newT[name] = [];
                console.log('adding: ' + name + ' as a favorite!');
                reqAddTransaction({id: name, time: currentTime, amount: 0});
            })
        } else {
            // remove as favorite
            // promt -> "You will also remove any transactions"
            confirmModal('.modal-confirm-favorite')
            .then(() => {
                $('.btn-favorite i').addClass('outline');
                isFavorite = false;
                
                reqGetTransactions()
                .then(t => {
                    if (t) {
                        removeTX(t, name)
                    }
                })

            })
            .catch(() => {
                // console.log('cancel')
            });
        }
    });
}
async function removeTX(t, name) {
    for (let i in t) {
        let txCoin = t[i];
        if (i == name) {
            for (tx of txCoin) {
                let id = tx.id;
                await reqRemoveTransaction(id);
            }
        }
    }
    updateTransactionList(name);
}
// async function refreshPrices() {
//     let tbody = $('#market-coins');
//     tbody.empty();

//     for (let i = 0; i < showCoins; i++) {
//         await getMarketCoins(1, i+1)
//         .then(data => {
//             // insert table row
//             for (id in data) {
//                 marketcoins[id] = data[id];

//                 let coin = data[id];
//                 let posneg = 'negative';
//                 if (coin.priceChange > 0) {
//                     posneg = 'positive';
//                 }            
//                $('.market-info-row').firstChild.val
//                 let tr = $(
//                     `<tr class="market-info-row">
//                         <td>${coin.rank}</td>
//                         ${nameTag}
//                         <td>${formatPrice(coin.price)}${currencySigns[fiat]}</td>
//                         <td class="${posneg}">${formatPerc(coin.percChange)}%</td>
//                         <td class="${posneg}">${formatPrice(coin.priceChange)}${currencySigns[fiat]}</td>
//                         <td>${formatMarketCap(coin.marketCap)}${currencySigns[fiat]}</td>
//                         <td>${formatMarketCap(coin.volume)}${currencySigns[fiat]}</td>
//                         <input type="hidden" value="${id}">
//                     </tr>`);

//                 tbody.append(
//                     tr
//                 );
//             }
//         })
//         .catch(error => {
//             console.log(error);
//         });
//     }
// }

function insertTempCoinInfoRow(tbody) {
    let tr = $(
        `<tr class="market-info-row" style="display: none;">
            <td class="rank extra"></td>
            <td class="coinname">
                <img class="market-info-img" src="${tempimagePath}">
                ...
            </td>
            <td class="price"></td>
            <td class="change"></td>
            <td class="changepercent extra"></td>
            <td class="mcap extra"></td>
            <td class="vol extra"></td>
            <input type="hidden" value="">
        </tr>`);
        tr
    tbody.append(
        tr
    ).show('normal');
    tr.show('normal');
}
function insertTempCoinInfoRowAnimated(tbody, delay) {
    let tr = $(
        `<tr class="market-info-row" style="display: none;">
            <td class="rank extra"></td>
            <td class="coinname">
                <img class="market-info-img" style="opacity: 50%;" src="${tempimagePath}">
                ...
            </td>
            <td class="price"></td>
            <td class="change"></td>
            <td class="changepercent extra"></td>
            <td class="mcap extra"></td>
            <td class="vol extra"></td>
            <input type="hidden" value="">
        </tr>`);
    tbody.append(
        tr
    );
    setTimeout(() => {
        tr.show('normal');
    }, delay);
}

function insertTempInfoRowsAnimated(tbody, amount, delay) {
    for (let i = 0; i < amount; i++) {
        insertTempCoinInfoRowAnimated(tbody, delay*(i+1));
    }
}
function loadMarketData(page) {
    return new Promise((resolve, reject) => {

        if (page <= coinPagesShowing) {
            reject('already showing this page');
        }

        if (page > coinPagesShowing) {
            coinPagesShowing = page;
        }

        let tbody = $('#market-coins');
        if (page == 1) {
            tbody.empty();
        }

        // for (let i = 0; i < coinsPerPage; i++) {
        //     insertTempCoinInfoRow(tbody);
        let delay = 50;
        // }
        insertTempInfoRowsAnimated(tbody, coinsPerPage, delay);

        let trs = tbody.children();
        getMarketCoins(coinsPerPage, page)
        .then(data => {
            // for (let i = coinsPerPage*(page-1); i < coinsPerPage; i++) {
                // insert table row
            let i = coinsPerPage*(page-1);

            for (id in data) {
                let _id = id;
                let _i = i;
                setTimeout(function() {
                    insertCoinInfoRow(_id, data, trs, _i, delay);
                }, (i-(coinsPerPage*(page-1)))*delay);

                i++;
            }
            resolve('loaded market data page ' + page);
        })
        .catch(error => {
            reject(error);
        });
    })
}
function insertCoinInfoRow(id, data, trs, i, delay) {
    marketcoins[id] = data[id];

    let coin = data[id];
    // console.log(coin);
    let posneg = 'negative';
    if (coin.priceChange > 0) {
        posneg = 'positive';
    }
    let tr = $(trs[i]);

    tr.children('.coinname').html(`
        <img class="market-info-img" src="${coin.img}">
        ${coin.name}
    `);

    setInfoRowValues(tr, coin, posneg);
    // tr.children('.rank').text(coin.rank);
    // tr.children('.price').text(`${formatPrice(coin.price)}${currencySigns[fiat]}`);
    // tr.children('.change').text(formatPerc(coin.percChange) + '%')
    // tr.children('.change').attr('class', `${posneg} change`);
    // tr.children('.changepercent').text(`${formatPrice(coin.priceChange)}${currencySigns[fiat]}`)
    // tr.children('.changepercent').attr('class', `${posneg} changepercent`);
    // tr.children('.mcap').text(`${formatMarketCap(coin.marketCap)}${currencySigns[fiat]}`)
    // tr.children('.vol').text(`${formatMarketCap(coin.volume)}${currencySigns[fiat]}`)
    
    tr.children('input').val(id);

    $(tr).on('click', function() {
        let id = this.children[this.childElementCount-1].value;
        showAssetPage(id);
    });

    $(tr).hide();
    $(tr).fadeIn(delay*10, function() {
        // console.log(i + ' complete')
    });
}
function setInfoRowValues(row, coin, posneg) {
    row.children('.rank').text(coin.rank);
    row.children('.price').text(`${formatPrice(coin.price)}${currencySigns[fiat]}`);
    row.children('.change').text(formatPerc(coin.percChange) + '%')
    row.children('.change').attr('class', `${posneg} change`);
    row.children('.changepercent').text(`${formatPrice(coin.priceChange)}${currencySigns[fiat]}`)
    row.children('.changepercent').attr('class', `${posneg} changepercent extra`);
    row.children('.mcap').text(`${formatMarketCap(coin.marketCap)}${currencySigns[fiat]}`)
    row.children('.vol').text(`${formatMarketCap(coin.volume)}${currencySigns[fiat]}`)
}
async function updateMarketData() {
    console.log('updating market data');
    let infoRows = $('#market-coins').children();

    await getMarketCoins(infoRows.length, 1)
    .then(data => {
        
        for (id in data) {
            marketcoins[id] = data[id];
            
            for (infoRow of infoRows) {
                let row = $(infoRow);
                let infoRowCoin = row.find('input').val();

                if (infoRowCoin == id) {
                    // update table row

                    let coin = data[id];
                    let posneg = 'negative';
                    if (coin.priceChange > 0) {
                        posneg = 'positive';
                    }

                    setInfoRowValues(row, coin, posneg);

                    // row.children('.rank').text(coin.rank)
                    // row.children('.price').text(formatPrice(coin.price) + currencySigns[fiat])
                    // row.children('.change').text(formatPerc(coin.percChange) + '%')
                    // row.children('.change').attr('class', `${posneg} change`);
                    // row.children('.changepercent').text(`${formatPrice(coin.priceChange)}${currencySigns[fiat]}`)
                    // row.children('.changepercent').attr('class', `${posneg} changepercent`);
                    // row.children('.mcap').text(`${formatMarketCap(coin.marketCap)}${currencySigns[fiat]}`)
                    // row.children('.vol').text(`${formatMarketCap(coin.volume)}${currencySigns[fiat]}`)
                }
            }
        }
    })
    .catch(error => {
        console.log(error);
    });
}
// async function updateMarketData() {
//     for (let i = 0; i < currentShowingCoins; i++) {
//         await getMarketCoins(1, i+1)
//         .then(data => {

//             let infoRows = $('#market-coins').children();
            
//             for (id in data) {
//                 marketcoins[id] = data[id];
                
//                 for (infoRow of infoRows) {
//                     let row = $(infoRow);
//                     let infoRowCoin = row.find('input').val();

//                     if (infoRowCoin == id) {
//                         // update table row

//                         let coin = data[id];
//                         let posneg = 'negative';
//                         if (coin.priceChange > 0) {
//                             posneg = 'positive';
//                         }

//                         row.children('.rank').text(coin.rank)
//                         row.children('.price').text(formatPrice(coin.price) + currencySigns[fiat])
//                         row.children('.change').text(formatPerc(coin.percChange) + '%')
//                         row.children('.change').attr('class', `${posneg} change`);
//                         row.children('.changepercent').text(`${formatPrice(coin.priceChange)}${currencySigns[fiat]}`)
//                         row.children('.changepercent').attr('class', `${posneg} changepercent`);
//                         row.children('.mcap').text(`${formatMarketCap(coin.marketCap)}${currencySigns[fiat]}`)
//                         row.children('.vol').text(`${formatMarketCap(coin.volume)}${currencySigns[fiat]}`)
//                     }
//                 }
//             }
//         })
//         .catch(error => {
//             console.log(error);
//         });

//     }
// }

function isLoggedIn() {
    return token ? true : false;
}

let marketDataInterval = null;
function showPage(s) {
    if (currentPage != s) {
        $(document).trigger('currentPageChanged', s);
    }

    if (s == PAGES.account) {
        updateAccountPage();
    }
    if (s == PAGES.home) {
        updateMarketData()

        if (marketDataInterval != null) {
            clearInterval(marketDataInterval);
        }
        marketDataInterval = setInterval(() => {
            if (currentPage != PAGES.home) {
                clearInterval(marketDataInterval);
                return;
            }

            updateMarketData()
        }, MARKET_DATA_REFRESH_TIME);
    }

    currentPage = s;

    for (p in PAGES) {
        $(PAGES[p]).hide();
    }
    $(s).show();
}

function toggleSignedInIcons(signedIn, username='') {
    $('.btn-openlogin').toggle(!signedIn);  // show when logged out
    $('.btn-logout').toggle(signedIn);      // show when logged in
    $('.account-label').toggle(signedIn);   // show when logged in

    if (signedIn) {
        $('.account-label').html('<i class="user icon"></i>' + username);
    }
}

function formatPrice(p) {
    if (Math.abs(p) < 1) {
        p = Math.floor(p*10000)/10000;
    } else if (Math.abs(p) < 1000) {
        p = Math.floor(p*100)/100;
    } else {
        p = formatMarketCap(Math.floor(p));
    }
    return p;
}
function formatMarketCap(m) {
    let s = ''+m;
    let sf = '';
    let j = 0;
    for (let i = s.length-1; i >= 0; i--) {
        if ((j % 3) == 0 && i != s.length-1) {
            sf = ',' + sf;
        }
        sf = s[i] + sf;
        j++;
    }
    return sf;
}
function formatPerc(p) {
    return Math.floor(p*100)/100;
}
function getUnixTime(date) {
    return new Date(date).valueOf() / 1000;
}
function getDateFromUnix(unixTime) {
    return new Date(unixTime);
}
function getDateFormatted(unixTime) {
    let d = new Date(unixTime);
    // Format: YYYY.MM.DDD, hh:mm

    let date = d.getFullYear() + "." + ("0" + (1+d.getMonth())).slice(-2) + "." + ("0" + d.getDate()).slice(-2) + ", " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2); 
    return date;
}
function getDateFormattedUserFriendly(unixTime) {
    let d = new Date(unixTime);

    const dateFormatOptions = { year: 'numeric', month: '2-digit', day: '2-digit' };
    return d.toLocaleDateString(undefined, dateFormatOptions) + ', ' + d.toLocaleTimeString(undefined);
}