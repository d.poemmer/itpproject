const MESSAGE_FADEINTIME    = 400;
const MESSAGE_FADEOUTIME    = 600;
const MESSAGE_DISPLAYTIME   = 2000;

const MESSAGES = {
    registered: 1, 
    loggedin: 2,
    loggedout: 3,
    generic: 4
}

let loginToPage = null;

let d = new Date();

function onLoginPressed() {
    if ($('.ui.form.form-login').form('is valid')) {
        let form = $('.ui.form.form-login');                    // get the login form
        let allFields = $(form).form('get values');             // get all values from the form

        // console.log(`trying to log in as ${un}:${pw}`);
        reqLogin(allFields['username'], allFields['password'])  // call loginrequest with the given username and password 
        .then(msg => {
            // hide the login form
            $('.ui.modal.modal-login').modal('hide');
            // show login message
            message(MESSAGES.loggedin);

            if (loginToPage != null) {
                if (loginToPage == PAGES.portfolio) {
                    showPortfolioPage();
                    navActiveButton('.btn-page-portfolio');
                    return;
                } else if (loginToPage == PAGES.account) {
                    navActiveButton('.btn-page-account');
                }

                showPage(loginToPage);

                loginToPage = null;
            }
        })
        .catch(error => {
            // clear the warnings list and add the error message
            $('.warning-login .list').empty();
            let errorMsg = error;
            $('.warning-login .list').append('<li>'+errorMsg+'</li>');
            $('.ui.form.form-login').addClass('warning');
        });
    }
}
function onRegisterPressed() {
    if ($('.ui.form.form-register').form('is valid')) {
        let form = $('.ui.form.form-register');             // get the registration form
        let allFields = $(form).form('get values');         // get all values from the form

        // console.log(`trying to register a new account ${un}:${em}:${pw}`);
        reqRegister(allFields['username'], allFields['email'], allFields['password'])   // call a registration request with the given inputs
        .then(msg => {
            console.log(msg);
            
            // hide the register form
            $('.ui.modal.modal-register').modal('hide');
            // show registered message
            message(MESSAGES.registered);
        })
        .catch(error => {
            console.log(error);

            // clear the warnings list and add the error message
            $('.warning-register .list').empty();
            let errorMsg = error;
            $('.warning-register .list').append('<li>'+errorMsg+'</li>');
            $('.ui.form.form-register').addClass('warning');
        });
    }
}

function confirmModal(id) {
    console.log('opening confirm dialog');
    return new Promise(function(resolve, reject) {
        $(id).modal('show');

        $(id).modal({
            onApprove: function() {
                resolve();
            },
            onDeny: function() {
                reject();
            }
        })
    });
}

$(document).ready(function() {
    // -- INIT MESSAGES -->
    $('.message-loggedin').hide();
    $('.message-registered').hide();
    $('.message-loggedout').hide();
    $('.message-generic').hide();
    // <-- INIT MESSAGES --

    // -- INIT BUTTONS -->
    // Init Open Login Modal Button
    $('.btn-openlogin').on('click', function() {
        resetLoginForm();
        $('.ui.modal.modal-login').modal('show');
    });
    // Init Open Register Modal Button
    $('.btn-openregister').on('click', function() {
        resetRegisterForm();
        $('.ui.modal.modal-register').modal('show');
    });
    // Init Logout Button
    $('.btn-logout').on('click', function() {
        $('.modal-logout')
            .modal('setting', 'closable', false)
            .modal('show');
    });
    // Init Logout Confirm Button
    $('.btn-logout-confirm').on('click', function() {
        logout();
    });
    // Init Open Account Page Button
    $('.account-label').on('click', function() {
        showPage(PAGES.account);
        navActiveButton('.btn-page-account');
    });
    // Init Home Button
    $('.btn-page-home').on('click', function() {
        showPage(PAGES.home);
        navActiveButton('.btn-page-home');
    });
    // Init Account Button
    $('.btn-page-account').on('click', function() {
        if (isLoggedIn()) {
            showPage(PAGES.account);
            navActiveButton('.btn-page-account');
        } else {
            resetLoginForm();
            $('.ui.modal.modal-login').modal('show');
            loginToPage = PAGES.account;

            // doesnt work
            // $('.ui.modal.modal-login').modal('show', {
            //     onApprove: () => {
            //         showPage(PAGES.account);
            //         console.log('approved');
            //     },
            //     onDeny: function() {
            //         console.log('denied');
            //     }
            // })
        }
    });
    // Init Portfolio Button
    $('.btn-page-portfolio').on('click', function() {
        if (isLoggedIn()) {
            navActiveButton('.btn-page-portfolio');
            showPortfolioPage();
        } else {
            resetLoginForm();
            $('.ui.modal.modal-login').modal('show');
            loginToPage = PAGES.portfolio;
        }
        // showPortfolioPage();
        // navActiveButton('.btn-page-portfolio');
    });
    // Init Notification Button
    $('#add-notification').click(function(){
        $('#modal-notification').modal('show');
    })
    // Init Confirm Notification for 
    $('#modal-notification .confirm-notification').on('click', function() {
        addNotification($('.sa-id').val(), $('#modal-notification .input-price').val());
        $('#modal-notification .input-price').val('');
    });

    // Init Transaction Button
    $('#add-transaction').click(function(){
        $('#modal-transaction').modal('show');
        let isoString = new Date().toISOString()
        let d = isoString.substring(0, (isoString.indexOf("T")|0) + 6|0);
        $('.input-date').val(d);
    })
    // Init Confirm Transaction for
    $('#modal-transaction .confirm-transaction').on('click', function() {
        let isSellSide = $('.transaction-side .sell').hasClass('active');
        let inputAmount = $('#modal-transaction .input-amount').val();
        let amount = Math.abs(inputAmount);

        if (isSellSide) {
            amount = -amount;
        }

        addTransaction($('.sa-id').val(), amount, $('#modal-transaction .input-date').val());
        $('#modal-transaction .input-amount').val('');
        $('#modal-transaction .input-date').val();
    });
    // Init transaction side buttons on trx modal
    $('.transaction-side .buy').on('click', function() {
        $('.transaction-side .sell').removeClass('active');
        $('.transaction-side .buy').addClass('active');
    })
    $('.transaction-side .sell').on('click', function() {
        $('.transaction-side .buy').removeClass('active');
        $('.transaction-side .sell').addClass('active');
    })


    // Init Timeframe Buttons
    $('.timeframeSingleAsset').on('click', function() {
        for (let buttonParent of $(this).parent().children()) {
            let button = $(buttonParent).children()[0];
            $(button).removeClass('active');
        }

        $($(this).children()[0]).addClass('active');

        let t = this.firstElementChild.innerHTML;
        let id = $('.sa-id').val();
        displaySingleAssetChart(id, t);
    });
    $('.timeframePortfolio button').on('click', function() {
        for (let buttonParent of $(this).parent().parent().children()) {
            let button = $(buttonParent).children()[0];
            $(button).removeClass('active')
        }
        $(this).addClass('active');

        let t = this.innerHTML;
        refreshPortfolioChart(t);
    });

    // Init Toggle 'Consider Transactions' Button
    $('.toggle-consider-tx').on('click', function() {
        let btn = $('.toggle-consider-tx');
        let isActive = btn.hasClass('active');

        isActive ? btn.removeClass('active') : btn.addClass('active');

        toggleConsiderTransactions(!isActive);
    })

    // Init remove account Button
    $('.btn-remove-account').on('click', function() {
        confirmModal('.modal-confirm-account')
        .then(() => {
            console.log('deleting account');
            reqDeleteAccount()
            .then(() => {
                message(MESSAGES.generic, 'Successfully removed account!');
                logout();
            })
            .catch(() => {
                message(MESSAGES.generic, 'Error while trying to remove account!');
            });
        })
        .catch(() => {
            // console.log('cancel')
        });
    });
    // Init change password button
    $('.btn-change-password').on('click', function() {
        $('.modal-changepassword').modal('show');
        $('.modal-changepassword').modal({
            onApprove: function() {
                let oldpw = $('.form-changepw .current-password').val();
                let newpw = $('.form-changepw .new-password').val();

                reqChangePassword(oldpw, newpw)
                .then(res => {
                    message(MESSAGES.generic, 'Successfully changed password!')
                })
                .catch(err => {
                    message(MESSAGES.generic, 'Password incorrect!')
                });

                $('.form-changepw .current-password').val('')
                $('.form-changepw .new-password').val('')
            },
            onDeny: function() {
                // console.log('cancelled');
                $('.form-changepw .current-password').val('')
                $('.form-changepw .new-password').val('')
            }
        });
    });
    // Init change username button
    $('.btn-change-username').on('click', function() {
        $('.modal-changeusername').modal('show');
        $('.modal-changeusername').modal({
            onApprove: function() {
                let newName = $('.form-changeun .newusername').val();

                reqChangeUsername(newName)
                .then(res => {
                    message(MESSAGES.generic, 'Successfully changed username!')
                    updateAccountPage();
                })
                .catch(err => {
                    message(MESSAGES.generic, 'Error changing username!')
                });

                $('.form-changeun .newusername').val('')
            },
            onDeny: function() {
                // console.log('cancelled');
                $('.form-changeun .newusername').val('')
            }
        });
    });
    // Init change email button
    $('.btn-change-email').on('click', function() {
        $('.modal-changeemail').modal('show');
        $('.modal-changeemail').modal({
            onApprove: function() {
                let newEmail = $('.form-changeem .newemail').val();

                reqChangeEmail(newEmail)
                .then(res => {
                    message(MESSAGES.generic, 'Successfully changed email!')
                    updateAccountPage();
                })
                .catch(err => {
                    message(MESSAGES.generic, 'Error changing email!')
                });

                $('.form-changeem .newemail').val('')
            },
            onDeny: function() {
                // console.log('cancelled');
                $('.form-changeem .newemail').val('')
            }
        });
    });
    // Init currency drop down on button click
    $('.ui.dropdown.currencydropdown').on('click', function() {
        $('.ui.dropdown.currencydropdown').dropdown({
            // On Dropdown value change
            onChange: function() {
                let newCurr = $('.ui.dropdown.currencydropdown').dropdown('get value');
                if (newCurr == fiat) {
                    // console.log('curr to change is the same');
                    return;
                }
    
                console.log('changing the default currency to: ' + newCurr);
                reqChangeCurrency(newCurr)
                .then(res => {
                    console.log(res.message);
                    updateAccountPage();
                })
            }
        });
    });

    // <-- INIT BUTTONS --

    // Init Login Form
    $('.ui.form.form-login')
    .form({
        fields: {
            username: {
                identifier  : 'username',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your username or e-mail'
                    }
                ]
            },
            password: {
                identifier  : 'password',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your password'
                    }
                ]
            }
        },
        onSuccess: function(event, fields) {
            // SubmitForm(fields);
            event.preventDefault();
        }
    });

    // Init Registration Form
    $('.ui.form.form-register')
    .form({
        fields: {
            username: {
                identifier  : 'username',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your username'
                    }
                ]
            },
            email: {
                identifier  : 'email',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your e-mail'
                    },
                    {
                        type   : 'email',
                        prompt : 'Please enter a valid e-mail'
                    }
                ]
            },
            password: {
                identifier  : 'password',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter your password'
                    },
                    {
                        type   : 'length[6]',
                        prompt : 'Your password must be at least 6 characters'
                    }
                ]
            }
        }
    });
});

function navActiveButton(active) {
    let buttons = document.querySelectorAll('nav .item');
    for (button of buttons) {
        $(button).removeClass('active');
    }

    $(active).addClass('active');
}

function message(type, msg = "") {
    if (type == MESSAGES.registered) {
        $('.message-registered').fadeIn(MESSAGE_FADEINTIME, function() {setTimeout(function() {$('.message-registered').fadeOut(MESSAGE_FADEOUTIME)}, MESSAGE_DISPLAYTIME)});
    }
    else if (type == MESSAGES.loggedin) {
        $('.message-loggedin').fadeIn(MESSAGE_FADEINTIME, function() {setTimeout(function() {$('.message-loggedin').fadeOut(MESSAGE_FADEOUTIME)}, MESSAGE_DISPLAYTIME)});
    }
    else if (type == MESSAGES.loggedout) {
        $('.message-loggedout').fadeIn(MESSAGE_FADEINTIME, function() {setTimeout(function() {$('.message-loggedout').fadeOut(MESSAGE_FADEOUTIME)}, MESSAGE_DISPLAYTIME)});
    }
    else if (type == MESSAGES.generic) {
        $('.message-generic div').text(msg);
        $('.message-generic').fadeIn(MESSAGE_FADEINTIME, function() {setTimeout(function() {$('.message-generic').fadeOut(MESSAGE_FADEOUTIME)}, MESSAGE_DISPLAYTIME)});
    }
}

function resetLoginForm() {
    $('.ui.form.form-login').form('reset');
    // $('.ui.form.form-login').removeClass('loading');
    $('.ui.form.form-login').removeClass('warning');
}

function resetRegisterForm() {
    $('.ui.form.form-register').form('reset');
    // $('.ui.form.form-register').removeClass('loading');
    $('.ui.form.form-register').removeClass('warning');
}