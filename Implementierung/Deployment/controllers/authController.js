const User          = require('../models/user');
const bcrypt        = require('bcryptjs');
const jwt           = require('jsonwebtoken');
const secretKey     = require('../middleware/secretkey');

const register = (req, res, next) => {
    bcrypt.hash(req.body.password, 10, function(err, hashedPass) {
        if (err) {
            res.json({
                error: err
            })
        }

        if (!req.body.name || !req.body.email) {
            console.log(`${req.body.name}:${req.body.email} failed to register!`);
            res
            .status(450)
            .json({
                message: 'Error occurred while trying to register a new user!'
            })
            return;
        }
        
        let user = new User ({
            name: req.body.name,
            email: req.body.email,
            password: hashedPass
        })

        
        user.save()
        .then(user => {
            console.log(`${user.name}:${user.email} just registered!`);

            res.json({
                message: 'User added successfully!'
            })
        })
        .catch(error => {
            console.log(`${user.name}:${user.email} register failed!`);

            res
            .status(450)
            .json({
                message: 'User already registered!'
            })
        })
    })
}

const login = (req, res, next) => {
    var username = req.body.username;
    var password = req.body.password;

    User.findOne({$or: [{email: username}, {name: username}]})
    .then(user => {
        if (user) {
            bcrypt.compare(password, user.password, function(err, result) {
                if (err) {
                    res
                    .status(450)
                    .json({
                        error: err
                    })
                }
                else if (result) {
                    console.log(`${user.name} just logged in!`);
                    let token = jwt.sign({userID: user._id, name: user.name}, secretKey.key(), {expiresIn: '24h'});
                    res
                    .cookie('access_token', token, {
                        expires: new Date(Date.now() + 12 * 3600000) // expire after 12h
                    })
                    .json({
                        message: 'Login successful!',
                        token // same as token: token
                    })
                }
                else {
                    res
                    .status(451)
                    .json({
                        message: 'username or password incorrect!'
                    })
                }
            })
        } 
        else {
            res
            .status(451)
            .json({
                message: 'username or password incorrect!'
            })
        }
    })
}


const changePassword = (req, res, next) => {
    var oldpw = req.body.oldpw;
    var newpw = req.body.newpw;

    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;

    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            bcrypt.compare(oldpw, user.password, function(err, result) {
                if (err) {
                    res
                    .status(450)
                    .json({
                        error: err
                    })
                }
                else if (result) {
                    console.log(`${user.name} is changing his password!`);
                    bcrypt.hash(newpw, 10, function(err, hashedPass) {
                        if (err) {
                            res.json({
                                error: err
                            })
                        }
                        
                        user.password = hashedPass;
                        user.save()
                        .then(user => {
                            console.log(`${user.name} changed his password!`);
                
                            res.json({
                                message: 'Password chganged successfully!'
                            })
                        })
                        .catch(error => {
                            console.log(`${user.name} password change failed!`);
                
                            res
                            .status(450)
                            .json({
                                message: 'Password not changed!'
                            })
                        })
                    })
                } 
                else {
                    res
                    .status(452)
                    .json({
                        message: 'Incorrect password!'
                    })
                }
            });
        } else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    });
}

module.exports = {
    register, login, changePassword
}