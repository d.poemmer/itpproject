const User          = require('../models/user');
const bcrypt        = require('bcryptjs');
const jwt           = require('jsonwebtoken');
const secretKey     = require('../middleware/secretkey');

const Api   = require('../middleware/coingeckoapi');

// responds with the name and email of the user that sent the request
const getUser = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;

    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            res.json({
                name: user.name,
                email: user.email,
                fiat: user.defaultCurrency
            })
        }
        else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    })
}

// responds with a dictionary of the users portfolio
const getPortfolio = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;
    
    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            let p = {};
            for (transactionId in user.transactions) {
                let tx = user.transactions[transactionId];

                if (p[tx['name']] == undefined) { p[tx['name']] = 0 }

                p[tx['name']] += tx['amount'];
            }
            console.log('returning portfolio to ' + user.name + '!');
            // console.log(p);

            res.json({
                portfolio: p
            })
        }
        else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    })
}

const getTransactions = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;

    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            let txDB = user.transactions;
            let tx = {};
            for (txId in txDB) {
                let t = txDB[txId];

                if (!tx[t['name']]) {tx[t['name']] = []}

                tx[t['name']].push({
                    'time': t['time'],
                    'amount': t['amount'],
                    'id': t['_id']
                });
            }

            console.log('returning transactions to ' + user.name + '!');

            res.json({
                transactions: tx
            })
        }
        else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    })
}

// adds an additional transaction for the user in the database
const addTransaction = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;

    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            // check new transaction
            let tx = req.body.transaction; // {id: "bitcoin", "time": Date, "amount": 100}
            if (!tx['id'] || !tx['time'] || !tx['amount']) {
                res
                .status(480)
                .json({
                    message: 'Invalid input parameters!'
                })
            }
            
            // append new tx to users transactions
            // transactions: ["name": "coinid", "time": "2020...", "amount": 100}]}
            user.transactions.push({name: tx['id'], time: tx['time'], amount: tx['amount']});
            user.save()
            .then(user => {
                console.log(user.name + ' added a transaction!');

                res.json({
                    message: 'Added a new Transaction!'
                })
            })
            .catch(error => {
                console.log(user.name + ' tried to update his transactions but failed!');
                
                res
                .status(472)
                .json({
                    message: 'An error occurred while adding a transaction!'
                })
            })
        }
        else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    });
}
// removes a specific transaction given the id
const removeTransaction = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;

    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            let id = req.body.transactionId;

            // TODO: do this with mongo statement
            let newTransactions = [];
            for (idx in user.transactions) {
                let tx = user.transactions[idx];
                if (tx['_id'] != id) {
                    newTransactions.push(tx);
                }
            }
            
            user.transactions = newTransactions;
            user.save()
            .then(user => {
                console.log(user.name + ' removed a transaction!');

                res.json({
                    message: 'Removed Transaction!'
                })
            })
            .catch(error => {
                console.log(user.name + ' tried to remove a transaction but failed!');
                
                res
                .status(472)
                .json({
                    message: 'An error occurred while removing a transaction!'
                })
            })
        }
        else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    })
}
// updates the portfolio for the user in the database
// const updateTransactions = (req, res, next) => {
//     const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
//     const decode = jwt.verify(token, secretKey.key());
    
//     req.user = decode;

//     User.findById(req.user.userID)
//     .then(user => {
//         if (user) {
//             let newTx = req.body.transactions;
//             // "coinid": [{"time": "2020...", "amount": 100}]
//             let tx = [];
//             for (coin in newTx) {
//                 for (txId in newTx[coin]) {
//                     let t = newTx[coin][txId];
//                     tx.push({name: coin, time: t['time'], amount: t['amount']});
//                 }
//             }
//             user.transactions = tx;

//             user.save()
//             .then(user => {
//                 console.log(`${user.name} updated his transactions!`);
//                 res.json({
//                     message: 'Updated Portfolio!'
//                 })
//             })
//             .catch(error => {
//                 console.log(`${user.name} could not update his transactions!`);
//                 res
//                 .status(471)
//                 .json({
//                     message: 'An error occurred while updating the portfolio!'
//                 })
//             })
//         }
//         else {
//             res
//             .status(451)
//             .json({
//                 message: 'No user found!'
//             })
//         }
//     })
// }

// responds with all notifications that the user set
const getNotifications = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;

    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            let n = {};
            for (idx in user.notifications) {
                let nentry = user.notifications[idx];
                if (n[nentry['name']]) {
                    n[nentry['name']].push({price: nentry['triggerPrice'], id: nentry['_id']});
                }
                else {
                    n[nentry['name']] = [{price: nentry['triggerPrice'], id: nentry['_id']}];
                }
            }

            console.log('returning notifications to ' + user.name + '!');

            res.json({
                notifications: n
            })
        }
        else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    })
}

// adds an additional notification for the user in the database
const addNotification = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;

    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            // check new notification
            let noti = req.body.notification; // {id: "bitcoin", price: 30000}
            if (!noti['id'] || !noti['price']) {
                res
                .status(480)
                .json({
                    message: 'Invalid input parameters!'
                })
            }
            
            // append new notification to n
            getFormattedNotification(noti, user)
            .then(n => {
                user.notifications.push(n);
                user.save()
                .then(user => {
                    console.log(user.name + ' updated his notifications!');

                    res.json({
                        message: 'Added a new Notification!'
                    })
                })
                .catch(error => {
                    console.log(user.name + ' tried to update his notifications but failed!');
                    
                    res
                    .status(472)
                    .json({
                        message: 'An error occurred while adding a notification!'
                    })
                })
            })
        }
        else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    });
}
function getFormattedNotification(n, user) {
    return new Promise(function(resolve, reject) {
        let updated;
        Api.getCoinPrice(n['id'], user.defaultCurrency)
        .then(price => {
            let isBelow = price < n['price'];
            
            updated = {name: n['id'], triggerPrice: n['price'], triggerAbove: isBelow};
            
            resolve(updated);
        })
        .catch(e => {
            reject(e);
        })
    })
}


// removes a specific notification given the id
const removeNotification = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;

    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            let id = req.body.notificationId;

            // TODO: do this with mongo statement
            let newNotifications = [];
            for (idx in user.notifications) {
                let n = user.notifications[idx];
                if (n['_id'] != id) {
                    newNotifications.push(n);
                }
            }
            
            user.notifications = newNotifications;
            user.save()
            .then(user => {
                console.log(user.name + ' removed a notification!');

                res.json({
                    message: 'Removed Notification!'
                })
            })
            .catch(error => {
                console.log(user.name + ' tried to remove a notification but failed!');
                
                res
                .status(472)
                .json({
                    message: 'An error occurred while removing a notifications!'
                })
            })
        }
        else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    })
}

const changeUsername = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;

    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            let newName = req.body.newUsername;
            let oldname = user.name;

            // Check if there is no user with that name already
            User.findOne({name: newName})
            .then(u => {
                if (u != null) {
                    res
                    .status(474)
                    .json({
                        message: 'Username already exists!'
                    });
                } else {
                    user.name = newName;
                    user.save()
                    .then(user => {
                        console.log(oldname + ' changed his username to ' + user.name + '!');
                        
                        res.json({
                            message: 'Changed Username!'
                        })
                    })
                    .catch(error => {
                        console.log(user.name + ' tried to change his Username but failed!');
                        
                        res
                        .status(473)
                        .json({
                            message: 'An error occurred while changing user name!'
                        })
                    })
                }
            });
        }
        else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    })
}

const changeEmail = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;

    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            let newMail = req.body.newEmail;
            let oldmail = user.email;

            // Check if there is no user with that name already
            User.findOne({email: newMail})
            .then(u => {
                if (u != null) {
                    res
                    .status(474)
                    .json({
                        message: 'Email already exists!'
                    });
                } else {
                    user.email = newMail;
                    user.save()
                    .then(user => {
                        console.log(oldmail + ' changed his email to ' + user.email + '!');
                        
                        res.json({
                            message: 'Changed Email!'
                        })
                    })
                    .catch(error => {
                        console.log(user.name + ' tried to change his Email but failed!');
                        
                        res
                        .status(473)
                        .json({
                            message: 'An error occurred while changing email!'
                        })
                    })
                }
            });
        }
        else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    })
}

const changeCurrency = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
    const decode = jwt.verify(token, secretKey.key());
    
    req.user = decode;

    User.findById(req.user.userID)
    .then(user => {
        if (user) {
            let newCurr = req.body.newCurrency;

            user.defaultCurrency = newCurr;
            user.save()
            .then(user => {
                console.log(user.name + ' changed his default currency to ' + user.defaultCurrency + '!');
                
                res.json({
                    message: 'Changed Default Currency!'
                })
            })
            .catch(error => {
                console.log(user.name + ' tried to change his default currency but failed!');
                
                res
                .status(474)
                .json({
                    message: 'An error occurred while changing default currency!'
                })
            })
        }
        else {
            res
            .status(451)
            .json({
                message: 'No user found!'
            })
        }
    })
}

// updates the notifications for the user in the database
// const updateNotifications = (req, res, next) => {
//     const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
//     const decode = jwt.verify(token, secretKey.key());
    
//     req.user = decode;

//     User.findById(req.user.userID)
//     .then(user => {
//         if (user) {
//             let newNotifications = req.body.notifications;
            
//             getUpdatedNotifications(newNotifications, user)
//             .then(updated => {
//                 user.notifications = updated;
//                 user.save()
//                 .then(user => {
//                     console.log(user.name + ' updated his notifications!');

//                     res.json({
//                         message: 'Updated Notifications!'
//                     })
//                 })
//                 .catch(error => {
//                     console.log(user.name + ' tried to update his notifications but failed!');
                    
//                     res
//                     .status(472)
//                     .json({
//                         message: 'An error occurred while updating the notifications!'
//                     })
//                 })
//             })
//         }
//         else {
//             res
//             .status(451)
//             .json({
//                 message: 'No user found!'
//             })
//         }
//     })
// }
// function getUpdatedNotifications(newNotifications, user) {
//     return new Promise(function(resolve, reject) {
//         let updated = [];
//         let i = 0;

//         for (notification in newNotifications) {
//             let noti = notification;
//             let p = newNotifications[noti];

//             Api.getCoinPrice(noti, user.defaultCurrency)
//             .then(price => {
//                 i+=1;
//                 let isBelow = price < p;
                
//                 updated.push({name: noti, triggerPrice: p, triggerAbove: isBelow});
                
//                 if (i >= Object.keys(newNotifications).length) {
//                     resolve(updated);
//                 }
//             })
//         }
//     })
// }


// show the list of users
const index = (req, res, next) => {
    User.find()
    .then(response => {
        res.json({
            response
        })
    })
    .catch(error => {
        res
        .status(450)
        .json({
            message: 'An error occured!'
        })
    })
}

// show single user
const show = (req, res, next) => {
    let userID = req.body.userID
    User.findById(userID)
    .then(response => {
        res.json({
            response
        })
    })
    .catch(error => {
        res
        .status(450)
        .json({
            message: 'An error occured!'
        })
    })
}

// add new user
const store = (req, res, next) => {
    bcrypt.hash(req.body.password, 10, function(err, hashedPass) {
        if (err) {
            res
            .status(450)
            .json({
                message: err
            })
        }
        
        let user = new User ({
            name: req.body.name,
            email: req.body.email,
            password: hashedPass
        })
        user.save()
        .then(user => {
            res.json({
                message: 'User added successfully!'
            })
        })
        .catch(error => {
            res
            .status(450)
            .json({
                message: 'An error occurred!'
            })
        })
    })
}

// update user
const update = (req, res, next) => {
    let userID = req.body.userID;

    let updatedData = {
        name: req.body.name,
        email: req.body.email
    }

    User.findByIdAndUpdate(userID, {$set: updatedData})
    .then(() => {
        res.json({
            message: 'User updated successfully!'
        })
    })
    .catch(error => {
        res
        .status(450)
        .json({
            message: 'An error occured!'
        })
    })
}

// delete user
const destroy = (req, res, next) => {
    let userID = req.user.userID;
    console.log(userID);
    User.findByIdAndRemove(userID)
    .then(() => {
        res.json({
            message: 'User removed successfully!'
        })
    })
    .catch(error => {
        res
        .status(450)
        .json({
            message: 'An error occured!'
        })
    })
}

module.exports = {
    getUser, getPortfolio, getTransactions, addTransaction, removeTransaction, getNotifications, addNotification, removeNotification, changeUsername, changeEmail, changeCurrency, index, show, store, update, destroy
}