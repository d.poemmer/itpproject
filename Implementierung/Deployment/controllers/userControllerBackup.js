// const User          = require('../models/user');
// const bcrypt        = require('bcryptjs');
// const jwt           = require('jsonwebtoken');
// const secretKey     = require('../middleware/secretkey');


// // responds with the name and email of the user that sent the request
// const getUser = (req, res, next) => {
//     const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
//     const decode = jwt.verify(token, secretKey.key());
    
//     req.user = decode;

//     User.findById(req.user.userID)
//     .then(user => {
//         if (user) {
//             res.json({
//                 name: user.name,
//                 email: user.email
//             })
//         }
//         else {
//             res
//             .status(451)
//             .json({
//                 message: 'No user found!'
//             })
//         }
//     })
// }

// // responds with a dictionary of the users portfolio
// const getPortfolio = (req, res, next) => {
//     const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
//     const decode = jwt.verify(token, secretKey.key());
    
//     req.user = decode;

//     User.findById(req.user.userID)
//     .then(user => {
//         if (user) {
//             let p = {};
//             for (portfolioEntry in user.portfolio) {
//                 let pentry = user.portfolio[portfolioEntry];
//                 p[pentry["name"]] = pentry["holdings"];
//             }

//             res.json({
//                 portfolio: p
//             })
//         }
//         else {
//             res
//             .status(451)
//             .json({
//                 message: 'No user found!'
//             })
//         }
//     })
// }

// // updates the portfolio for the user in the database
// const updatePortfolio = (req, res, next) => {
//     const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
//     const decode = jwt.verify(token, secretKey.key());
    
//     req.user = decode;

//     User.findById(req.user.userID)
//     .then(user => {
//         if (user) {
//             let newPortfolio = req.body.portfolio;
//             let p = [];

//             for (portfolioEntry in newPortfolio) {
//                 p.push({name: portfolioEntry, holdings: newPortfolio[portfolioEntry]});
//             }
//             user.portfolio = p;

//             user.save()
//             .then(user => {
//                 console.log(`${user.name} updated his portfolio!`);
//                 res.json({
//                     message: 'Updated Portfolio!'
//                 })
//             })
//             .catch(error => {
//                 console.log(`${user.name} could not update his portfolio!`);
//                 res
//                 .status(471)
//                 .json({
//                     message: 'An error occurred while updating the portfolio!'
//                 })
//             })
//         }
//         else {
//             res
//             .status(451)
//             .json({
//                 message: 'No user found!'
//             })
//         }
//     })
// }

// // responds with all notifications that the user set
// const getNotifications = (req, res, next) => {
//     const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
//     const decode = jwt.verify(token, secretKey.key());
    
//     req.user = decode;

//     User.findById(req.user.userID)
//     .then(user => {
//         if (user) {
//             let n = {};
//             for (notification in user.notifications) {
//                 let nentry = user.notifications[notification];
//                 n[nentry['name']] = nentry['triggerPrice'];
//             }

//             res.json({
//                 notifications: n
//             })
//         }
//         else {
//             res
//             .status(451)
//             .json({
//                 message: 'No user found!'
//             })
//         }
//     })
// }

// // updates the notifications for the user in the database
// const Api   = require('../middleware/coingeckoapi');
// const updateNotifications = (req, res, next) => {
//     const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
//     const decode = jwt.verify(token, secretKey.key());
    
//     req.user = decode;

//     User.findById(req.user.userID)
//     .then(user => {
//         if (user) {
//             let newNotifications = req.body.notifications;
            
//             getUpdatedNotifications(newNotifications, user)
//             .then(updated => {
//                 user.notifications = updated;
//                 user.save()
//                 .then(user => {
//                     res.json({
//                         message: 'Updated Notifications!'
//                     })
//                 })
//                 .catch(error => {
//                     res
//                     .status(472)
//                     .json({
//                         message: 'An error occurred while updating the notifications!'
//                     })
//                 })
//             })
//         }
//         else {
//             res
//             .status(451)
//             .json({
//                 message: 'No user found!'
//             })
//         }
//     })
// }
// function getUpdatedNotifications(newNotifications, user) {
//     return new Promise(function(resolve, reject) {
//         let updated = [];
//         let i = 0;

//         for (notification in newNotifications) {
//             let noti = notification;
//             let p = newNotifications[noti];

//             Api.getCoinPrice(noti, user.defaultCurrency)
//             .then(price => {
//                 i+=1;
//                 let isBelow = price < p;
                
//                 updated.push({name: noti, triggerPrice: p, triggerAbove: isBelow});
                
//                 if (i >= Object.keys(newNotifications).length) {
//                     resolve(updated);
//                 }
//             })
//         }
//     })
// }



// // show the list of users
// const index = (req, res, next) => {
//     User.find()
//     .then(response => {
//         res.json({
//             response
//         })
//     })
//     .catch(error => {
//         res
//         .status(450)
//         .json({
//             message: 'An error occured!'
//         })
//     })
// }

// // show single user
// const show = (req, res, next) => {
//     let userID = req.body.userID
//     User.findById(userID)
//     .then(response => {
//         res.json({
//             response
//         })
//     })
//     .catch(error => {
//         res
//         .status(450)
//         .json({
//             message: 'An error occured!'
//         })
//     })
// }

// // add new user
// const store = (req, res, next) => {
//     bcrypt.hash(req.body.password, 10, function(err, hashedPass) {
//         if (err) {
//             res
//             .status(450)
//             .json({
//                 message: err
//             })
//         }
        
//         let user = new User ({
//             name: req.body.name,
//             email: req.body.email,
//             password: hashedPass
//         })
//         user.save()
//         .then(user => {
//             res.json({
//                 message: 'User added successfully!'
//             })
//         })
//         .catch(error => {
//             res
//             .status(450)
//             .json({
//                 message: 'An error occurred!'
//             })
//         })
//     })
// }

// // update user
// const update = (req, res, next) => {
//     let userID = req.body.userID;

//     let updatedData = {
//         name: req.body.name,
//         email: req.body.email
//     }

//     User.findByIdAndUpdate(userID, {$set: updatedData})
//     .then(() => {
//         res.json({
//             message: 'User updated successfully!'
//         })
//     })
//     .catch(error => {
//         res
//         .status(450)
//         .json({
//             message: 'An error occured!'
//         })
//     })
// }

// // delete user
// const destroy = (req, res, next) => {
//     let userID = req.body.userID;
//     User.findByIdAndRemove(userID)
//     .then(() => {
//         res.json({
//             message: 'User removed successfully!'
//         })
//     })
//     .catch(error => {
//         res
//         .status(450)
//         .json({
//             message: 'An error occured!'
//         })
//     })
// }

// module.exports = {
//     getUser, getPortfolio, updatePortfolio, getNotifications, updateNotifications, index, show, store, update, destroy
// }