let nodemailer = require('nodemailer');

let path        = require('path');
let fs          = require('fs');
let handlebars  = require('handlebars');


let sender = 'plutoscrypto@gmail.com';

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: sender,
        pass: 'Lk9Z*5gXp#2RztVOnkh7%9'
    }
});

let currencySigns = {
    "usd": "$",
    "eur": "€"
};

function sendMail(email, coin, triggerPrice, fiat) {
    const filePath = path.resolve(__dirname, './emails/pricealert.html');
    const source = fs.readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);
    
    let message = `You are receiving this email, because you wanted to be notified when ${coin} hits ${triggerPrice}${currencySigns[fiat]}!`;
    
    const replacements = {
        message,
        headermessage: `${coin} hit ${triggerPrice}${currencySigns[fiat]}!`
    };
    const htmlToSend = template(replacements);
    
    var mailOptions = {
        from: sender,
        to: email,
        subject: 'Plutos - Price Alert',
        text: message,
        html: htmlToSend
    };
    
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log('Sending Email to ' + email + ' failed. error: ');
            console.log(error);
        } else {
            // console.log('Email sent: ' + info.response);
            console.log(`Email notification sent to ${email}, because ${coin} hit ${triggerPrice}!`);
        }
    });
}

module.exports = {
    sendMail
}