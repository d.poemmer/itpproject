const User          = require('../models/user');
const Api           = require('../middleware/coingeckoapi');
const Email         = require('../middleware/email');

const CheckingInervalMinutes = 2;

function startCheckingInterval() {
    console.log('checking user notifications...');
    checkNotifications();
    setTimeout(startCheckingInterval, 1000*60*CheckingInervalMinutes);
};

function checkNotifications() {
    getNotifications()
    .then(n => {
        let notifications = n;
        
        if (notifications.length > 0) {
            notifications.forEach(noti => {

                let n = noti['notification'];
                Api.getCoinPrice(n['name'], noti['fiat'])
                .then(price => {
                    // check if the user wanted it to trigger if price is below or above the trigger price
                    if ((price > n['triggerPrice'] && n['triggerAbove'])
                        || (price < n['triggerPrice'] && !n['triggerAbove'])) {
                        // Notification should trigger
                        sendNotification(noti);
                        
                        // remove Notification
                        removeNotification(noti);
                    }
                })
                .catch(error => {
                    console.log(`Error while trying to get price of ${n['name']}!`);
                    console.log(error);
                })
            })
        }
    });
}
function sendNotification(n) {
    let email   = n['email'];
    let coin    = n['notification']['name'];
    let trigger = n['notification']['triggerPrice'];
    let fiat    = n['fiat'];

    Email.sendMail(email, coin, trigger, fiat);
}

function removeNotification(n) {
    User.findOne({email: n['email']})
    .then(user => {
        let newNotis = [];
        let idToRemove = n['notification']['_id'];

        // add all notifications to a new array except for the one to delete
        for (nIndex in user.notifications) {
            let notification = user.notifications[nIndex];
            if (notification['_id'] + '' != idToRemove + '') {
                newNotis.push(notification)
            }
        }
        user.notifications = newNotis;
        user.save()
        .then(user => {
            console.log(`removed executed notification from user: ${user.name}!`);
        })
        .catch(error => {
            console.log('An error occurred while trying to remove an executed notification!');
            console.log(error);
        })
    })
    .catch(error => {
        console.log('User not found');
    })
}

function getNotifications() {
    return new Promise(function(resolve, reject) {
        User.find()
        .then(users => {
            let notifications = [];

            users.forEach(user => {
                for (n in user.notifications) {
                    notifications.push({
                        email: user.email,
                        notification: user.notifications[n],
                        fiat: user.defaultCurrency
                    });
                }
            });
            
            resolve(notifications);
        });
    });
}

module.exports = {
    startCheckingInterval
}