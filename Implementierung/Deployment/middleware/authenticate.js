const jwt   = require('jsonwebtoken');
const User  = require('../models/user');
const secretKey     = require('../middleware/secretkey');

const authenticate = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
        const decode = jwt.verify(token, secretKey.key());

        req.user = decode;
        next();
    }
    catch (error) {
        res
        .status(460)
        .json({
            message: 'Authentication failed!'
        })
    }
}

const authenticateAdmin = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1]; // Bearer asdfTOKEN1234
        const decode = jwt.verify(token, secretKey.key());
        
        req.user = decode;

        User.findOne({name: req.user.name})
        .then(user => {
            if (user.isAdmin) {
                next();
            }
            else {
                res
                .status(461)
                .json({
                    message: 'Action prohibited!'
                })
            }
        })
    }
    catch (error) {
        res
        .status(460)
        .json({
            message: 'Authentication failed!'
        })
    }
}

module.exports = {
    authenticate, authenticateAdmin
}