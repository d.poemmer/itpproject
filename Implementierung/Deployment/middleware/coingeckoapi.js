const axios = require('axios');

function getCoinPrice(coin, fiat) {
    return new Promise(function(resolve, reject) {

        let callURL = "https://api.coingecko.com/api/v3/simple/price?ids="+coin+"&vs_currencies="+fiat;

        axios.get(callURL)
        .then(response => {
            let price = response.data[coin][fiat];
            resolve(price);
        })
        .catch(error => {
            reject(error);
        });
    });
}

module.exports = {
    getCoinPrice
}