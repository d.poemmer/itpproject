const express = require('express');
const router  = express.Router();

const userController = require('../controllers/userController');
const authenticate   = require('../middleware/authenticate');

router.get('/getUser', authenticate.authenticate, userController.getUser);
router.get('/getPortfolio', authenticate.authenticate, userController.getPortfolio);
router.get('/getTransactions', authenticate.authenticate, userController.getTransactions);
router.post('/addTransaction', authenticate.authenticate, userController.addTransaction);
router.post('/removeTransaction', authenticate.authenticate, userController.removeTransaction);
router.get('/getNotifications', authenticate.authenticate, userController.getNotifications);
router.post('/addNotification', authenticate.authenticate, userController.addNotification);
router.post('/removeNotification', authenticate.authenticate, userController.removeNotification);
router.get('/deleteUser', authenticate.authenticate, userController.destroy);
router.post('/changeUsername', authenticate.authenticate, userController.changeUsername);
router.post('/changeEmail', authenticate.authenticate, userController.changeEmail);
router.post('/changeCurrency', authenticate.authenticate, userController.changeCurrency);

router.get('/', authenticate.authenticateAdmin, userController.index);
router.post('/show', authenticate.authenticateAdmin, userController.show);
router.post('/store', authenticate.authenticateAdmin, userController.store);
router.post('/update', authenticate.authenticateAdmin, userController.update);
router.post('/delete', authenticate.authenticateAdmin, userController.destroy);

module.exports = router