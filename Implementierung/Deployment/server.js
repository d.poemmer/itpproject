const express     = require('express');
const mongo       = require('mongodb');
const mongoose    = require('mongoose');
const morgan      = require('morgan');
const bodyParser  = require('body-parser');

const userRoute = require('./routes/user');
const authRoute = require('./routes/auth');

const notifications = require('./middleware/notifications');

const port = 8080;

//var assert = require('assert');
var url = 'mongodb://localhost:27017/test';

// connect to the database
mongoose.connect('mongodb://localhost:27017/plutos', {useNewUrlParser: true, useUnifiedTopology: true});
// remove depreciation warnings
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

const db = mongoose.connection;


db.on('error', (err) => {
    console.log(err);
});
db.once('open', () => {
    console.log('DB Connection Established!');

    // start checking if notifications trigger
    notifications.startCheckingInterval();
});


// start server
const app = express();
app.use(express.static('public')); // deliver public folder to client
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});

app.use('/api/user', userRoute);
app.use('/api', authRoute);



const email = require('./middleware/email');
// email.sendMail('actuallydom@gmail.com', 'bitcoin', 40000);