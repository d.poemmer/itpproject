const mongoose          = require('mongoose');
const Schema            = mongoose.Schema;
// const PortfolioEntry    = require('./portfolioentry');
const Transaction    = require('./transaction');
const Notification      = require('./notification');

// const userSchema    = new Schema({
//     name: String,
//     email: String
// }, {timestamps: true});


let userSchema = new Schema({
    name: {
      type: String,
      unique: true
    },
    email: {
      type: String,
      unique: true
    },
    password: {
        type: String
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    transactions: {
        type: [Transaction],
        default: []
    },
    defaultCurrency: {
        type: String,
        default: "usd"
    },
    notifications: {
        type: [Notification],
        default: []
    }
}, {timestamps: true});

const User = mongoose.model('User', userSchema);
module.exports = User;