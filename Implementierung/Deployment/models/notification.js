const mongoose      = require('mongoose');
const Schema        = mongoose.Schema;

let notificationSchema = new Schema({
    name: {
        type: String
    },
    triggerPrice: {
        type: Number
    },
    triggerAbove: {
        type: Boolean,
        default: true
    }
});

// const Notification = mongoose.model('Notification', notificationSchema);
module.exports = notificationSchema;