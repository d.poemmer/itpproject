const mongoose      = require('mongoose');
const Schema        = mongoose.Schema;

let portfolioEntrySchema = new Schema({
    name: {
        type: String
    },
    holdings: {
        type: Number
    }
});

// const PortfolioEntry = mongoose.model('PortfolioEntry', portfolioEntrySchema);
module.exports = portfolioEntrySchema;