const mongoose      = require('mongoose');
const Schema        = mongoose.Schema;

let transactionSchema = new Schema({
    name: {
        type: String
    },
    time: {
        type: Date,
        default: Date.now
    },
    amount: {
        type: Number
    }
});

// const PortfolioEntry = mongoose.model('PortfolioEntry', portfolioEntrySchema);
module.exports = transactionSchema;