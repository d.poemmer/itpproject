let fiat = "usd"; // "eur"
let currencySigns = {
    "usd": "$",
    "eur": "€"
};
let portfolioHoldings = {};
let portfolio = {};
let coins = {};
let supportedCoins;


$(document).ready(function() {
    // TODO: get this array from server
    supportedCoins = ["bitcoin", "etherium", "tether", "ripple", "bitcoincash", "binancecoin", "chainlink", "polkadot", "cardano", "litecoin"];
    
    getMarketCoins(10, 1, marketDataLoaded);
    // Top 10 to 20 coins
    // getMarketCoins(10, 2, function(c) {console.log(c)});

    // TODO: get portpolioholdings from server, then call display
    portfolioHoldings = {
        "bitcoin": 100.372,
        "ripple": 56000,
        "ethereum": 2.4
    };


    // TODO: refresh prices (portfolio after 10 mins)

    //setTimeout(function() {
    //    displayPortfolio(portfolio);
    //}, 1000);

    // TODO: get chart data and plot graph
    getChartData("bitcoin", "14d", chartDataLoaded);
});


// to get the first 10: getMarketCoins(10, 1)
function getMarketCoins(amount, page, callback) {
    let callURL = "https://api.coingecko.com/api/v3/coins/markets?vs_currency="+fiat+"&order=market_cap_desc&per_page="+amount+"&page="+page+"&sparkline=false&price_change_percentage=24h"; // get from api

    let responseObject = $.getJSON(callURL, apicallready);
    
    // responseObject error detection
    // responsObject

    function apicallready(data) {
        let coinsFormatted = {};

        for (entry in data) {
            let coin = data[entry];
            coinsFormatted[coin.id] = formatCoin(coin);
            // {
            //     "name": coin.name,
            //     "price": coin.current_price,
            //     "priceChange": coin.price_change_24h,
            //     "marketCap": coin.market_cap
            //     // , ... (can get more if needed)
            // };
        }
        
        callback(coinsFormatted);
    }
}
function getPortfolioCoins(portfolio, callback) {
    let ids = "";
    let s = "%2C";
    for (coin in portfolio) {
        if (ids != "") {
            ids += s;
        }
        ids += coin;
    }

    let callURL = "https://api.coingecko.com/api/v3/coins/markets?vs_currency="+fiat+"&ids="+ids+"&order=market_cap_desc&per_page=100&page=1&sparkline=false&price_change_percentage=24h"


    let responseObject = $.getJSON(callURL, apicallready);

    // responseObject error detection
    // responsObject

    function apicallready(data) {
        let coinsFormatted = {};

        for (entry in data) {
            let coin = data[entry];
            coinsFormatted[coin.id] = formatCoin(coin);
        }

        callback(coinsFormatted);
    }
}

function marketDataLoaded(c) {
    coins = c;

    displayCoins(coins);

    getPortfolioCoins(portfolioHoldings, portfolioDataLoaded);
}
function portfolioDataLoaded(p) {
    portfolio = p;

    updatePortfolioPrices(portfolioHoldings);
    // when updating is finished, call displayPortfolio (not with a callback)
    displayPortfolio(portfolio);
}
function chartDataLoaded(coindata) {
    // coindata: minimum array length: 12
    // maybe create function for max length of about 48
    coindata = limitDataPoints(coindata, 48);

    let timestamps = [];
    coindata.forEach(element => timestamps.push(getDateFormatted(element[0])));
    let prices = [];
    coindata.forEach(element => prices.push(Math.round(parseFloat(element[1])*100)/100));

    displayChart(timestamps, prices);
}

function displayChart(timestamps, prices) {
    let ctx = $('#myChart');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: timestamps,
            datasets: [{
                label: 'not shown',
                data: prices,
                backgroundColor: [
                    '#95a5a6',
                ],
                borderColor: function(context) {
                    return '#323232';
                    // let index = context.dataIndex;
                    // let value = context.dataset.data[index];
                    // return index % 2 ? 'yellow' : 'black';
                },
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false
            },
            layout: {
                padding: 25 // {left: 50, right: 0, top: 0, bottom: 0}
            },
            elements: {
                point: {
                    radius: 0
                }
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                custom: function(tooltip) {
                    if (!tooltip) return;
                    tooltip.displayColors = false;
                },
                callbacks: {
                    label: function(tooltipItem, data) {
                        // var label = data.datasets[tooltipItem.datasetIndex].label || '';
    
                        // if (label) {
                        //     label += ': ';
                        // }
                        let label = tooltipItem.xLabel;
                        label += ': ' + tooltipItem.yLabel + ' ' + currencySigns[fiat];
                        return label;
                    },
                    title: function(tooltipItem, data) {
                        return;
                    }
                }
            },
            scales: {
                xAxes: [{
                    // TODO: https://www.chartjs.org/docs/latest/axes/cartesian/time.html
                    // type: 'time',
                    // distribution: 'series'

                    // time: {
                    //     unit: 'month',
                    //     displayFormats: {
                    //         quarter: 'MMM YYYY'
                    //     }
                    // }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        callback: function(label, index, labels) {
                            return label + " " + currencySigns[fiat];
                        }
                    }
                }]
            }
        }
    });
} 

function updatePortfolioPrices(p) {
    for (coin in p) {
        let h = p[coin];
        
        let price = 0;
        if (coins[coin] != null) {
            price = coins[coin]["price"];
            updatePortfolioEntry(coin, h, price);
        }
        else {
            console.log("cannot display " + coin + " in portfolio, data missing");
            // apiSinglePrice(coin, function(p) {
            //     updatePortfolioEntry(coin, h, p);
            // });
        }
    }

    console.log("every coin should be updated by now");
}
function updatePortfolioEntry(coin, h, price) {
    console.log("Updating: " + coin);
    portfolio[coin] = { holdings: h, value: (Math.round(price*h*100)/100).toFixed(2) };
}


function displayPortfolio(p) {
    console.log(p);
    console.log("---> displaying portfolio of "+Object.keys(p).length+" coins!");

    for (coin in p) {
        let name = coin;
        let entry = p[coin];
        let holdings = entry["holdings"];
        let value = entry["value"];

        console.log(name + ": " + value);

        // if the portfolio entry doesnt exist, add it to the list
        if (!$("#"+name+"_portfolio").length) {
            $("#portfolio").append("<li id=\""+name+"_portfolio\"><p>"+name+": </p><p>"+holdings+"</p><p>"+value+currencySigns[fiat]+"</p></li>");
        }
        // otherwhise, edit the current entry
        else {
            let p = $("#"+name+"_portfolio p:last-child");
            p.text(value + currencySigns[fiat]);
        }
    }
}

function displayCoins(c) {
    console.log("---> displaying " + c.length + " coins!");
    for (idx in c) {
        let name = c[idx].name;
        let price = c[idx].price;
        //console.log(name + ": " + price);
        $("#topcoins").append("<li><p>"+name+": </p><p id=\""+name+"_price"+"\">"+price+currencySigns[fiat]+"</p></li>");
    }
}

function getChartData(coin, timeframe, callback) {
    // calculate from/to date
    let currentTime = new Date();
    let toDate = getUnixTime(currentTime);
    let fromDate = currentTime;

    // calculate time back to set the desired timeframe
    let s = timeframe.substr(timeframe.length-1, 1);
    let span = parseInt(timeframe.substr(0, timeframe.length-1));
    if (s == 'h') {
        fromDate.setHours(fromDate.getHours() - span);
    }
    else if (s == 'd')
        fromDate.setDate(fromDate.getDate() - span);
    else if (s == 'm')
        fromDate.setMonth(fromDate.getMonth() - span);
    else if (s == 'y')
        fromDate.setFullYear(fromDate.getFullYear() - span);
    console.log(fromDate);

    // convert to unix time
    fromDate = getUnixTime(fromDate);

    // call the api
    let callURL = "https://api.coingecko.com/api/v3/coins/"+coin+"/market_chart/range?vs_currency=usd&from="+fromDate+"&to="+toDate;
    
    let responseObject = $.getJSON(callURL, apicallready);

    function apicallready(data) {
        // interval:
        // - 1 m for < 1 day
        // - 1 h for < 90 days
        // - 1 d for > 90 days
        let chartData = data["prices"];
        callback(chartData);
    }
}


function apiSinglePrice(coin, callback) {
    let callURL = "https://api.coingecko.com/api/v3/simple/price?ids="+coin+"&vs_currencies="+fiat;

    let responseObject = $.getJSON(callURL, apicallready);
    
    function apicallready(data) {
        let price = data[coin][fiat];
        callback(price);
    }
}
