function getUnixTime(date) {
    return new Date(date).valueOf() / 1000;
}
function getDateFromUnix(unixTime) {
    return new Date(unixTime);
}
function getDateFormatted(unixTime) {
    let d = new Date(unixTime);
    // return d;
    
    let date = d.getFullYear() + "." + ("0" + d.getMonth()).slice(-2) + "." + ("0" + d.getDay()).slice(-2) + ", " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2); 
    return date;
}


function formatCoin(coin) {
    return {
        "name": coin.name,
        "price": coin.current_price,
        "priceChange": coin.price_change_24h,
        "marketCap": coin.market_cap
        // , ... (can get more if needed)
    };
}

function limitDataPoints(data, limit) {
    let newData = [];
    let size = data.length;

    if (size > limit) {
        let index = size-1;
        let n = size/limit;
        for (let i = limit-1; i >= 0; i--) {
            newData.unshift(data[Math.floor(index)]);
            index -= n;
        }
    }
    else {
        newData = data;
    }

    return newData;
}